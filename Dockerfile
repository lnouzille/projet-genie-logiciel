# Utilisez une image comme base
FROM  maven:3.8.8-eclipse-temurin-17

# Copiez le fichier JAR Spring Boot dans l'image Docker
COPY target/demo-0.0.1-SNAPSHOT.jar /app.jar

# Commande pour démarrer l'application Spring Boot lorsqu'un conteneur est lancé
CMD ["java", "-jar", "/app.jar"]
