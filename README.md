# Projet de Génie Logiciel 

**Etudiants :** Lorie NOUZILLE et Tom AGONNOUDE

### Description du projet ###

L'objectif principal de ce projet est de **concevoir et de déployer une application web backend capable de scanner les répertoires et les fichiers d'un système de fichiers à partir d'un répertoire parent**.                        
Pour mettre en œuvre cette fonctionnalité de scan, trois patrons de conception sont utilisés : la **stratégie**, le **composite** et l'**observateur**, qui seront détaillés ultérieurement.                   
Deux options sont possibles pour le système de fichiers : le système de fichiers local ou le bucket S3 d'AWS.              

### Installation ###
#### Ubuntu ####
```bash
apt install openjdk-17-jdk openjdk-17-jre
```

#### Mac ####
Aller sur https://docs.oracle.com/en/java/javase/17/install/installation-jdk-macos.html#GUID-2FE451B0-9572-4E38-A1A5-568B77B146DE

#### Tester la bonne installation ####
```bash
java -version
```
#### Lancer un conteneur postgres ###
* Port : 5433
* Base de données : db_springboot
* Utilisateur : db_user
* Mot de passe : db_password     

```
sudo docker run --name postgres_projet -e POSTGRES_DB=db_springboot -e POSTGRES_USER=db_user -e POSTGRES_PASSWORD=db_password -p 5433:5432 -d postgres
```

### Utilisation ###
#### Lancer le programme ####
```bash
./mvnw spring-boot:run
```

#### Lancer les tests ####
```bash 
./mvnw test 
```

### Création et lancement d'une image docker ###

#### Créer une image docker à partir du code ####
```bash
docker build -t <nom de l'image> .
```

#### Instancier un conteneur de cette image ####
```bash
docker compose up
```

#### Arrêter et supprimer les conteneurs ####
```bash
docker compose down
```