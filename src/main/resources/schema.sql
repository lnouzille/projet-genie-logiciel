DROP TABLE IF EXISTS scan;
DROP TABLE IF EXISTS fichier; 
DROP TABLE IF EXISTS scan_fichier;
DROP TABLE IF EXISTS statistiques_scan;

CREATE TABLE scan (
    id_scan INT AUTO_INCREMENT PRIMARY KEY,
    repertoire_initial VARCHAR(250) NOT NULL,
    scan_local VARCHAR(5) NOT NULL CHECK (scan_local IN ('true', 'false')),
    nb_max_fichiers INT NOT NULL,
    profondeur_max_arbo INT NOT NULL,
    filtre_nom_fichier VARCHAR(250),
    filtre_type_fichier VARCHAR(250),
    date_scan TIMESTAMP NOT NULL,
    temps_execution INT NOT NULL
);

CREATE TABLE fichier (
    id_fichier INT AUTO_INCREMENT PRIMARY KEY,
    nom_fichier VARCHAR(250) NOT NULL, 
    date_modification TIMESTAMP NOT NULL,
    poids FLOAT NOT NULL,
    type_fichier VARCHAR(250) NOT NULL, 
    repertoire_contenant VARCHAR(250) NOT NULL
);

CREATE TABLE scan_fichier (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_scan INT,
    id_fichier INT,
    FOREIGN KEY (id_scan) REFERENCES scans(id_scan),
    FOREIGN KEY (id_fichier) REFERENCES fichiers(id_fichier)
);

CREATE TABLE statistiques_scan (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_scan INT, 
    FOREIGN KEY (id_scan) REFERENCES scans(id_scan),
    temps_execution_total_repertoires INT NOT NULL;
    nb_repertoires_scannes INT NOT NULL;
    temps_execution_total_fichiers INT NOT NULL;
    nb_fichiers_scannes INT NOT NULL;
)
