package fr.ensai.demo.controller;

import java.util.Iterator;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.ensai.demo.dto.DupliquerScanChampDto;
import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.model.ScanFichier;
import fr.ensai.demo.service.ComparaisonScansService;
import fr.ensai.demo.service.ScanService;
import jakarta.validation.Valid;

@RestController
public class ScanController {

    @Autowired
    private ScanService scanService;
    @Autowired
    private ComparaisonScansService comparaisonScansService;

    /**
     * Obtenir les informations de tous les scans stockés dans la table Scan.
     * 
     * @return Iterable<Scan> : informations des scans.
     */
    @GetMapping("/scans")
    public Iterable<Scan> getScans() {
        return scanService.getScans();
    }

    /**
     * Obtenir les informations d'un scan de la table Scan.
     * 
     * @param id : identifiant du scan.
     * @return Optional<Scan> : informations du scan s'il est présent dans la table.
     * @throws ResponseStatusException :
     *                                 Si aucun scan correspondant à l'identifiant
     *                                 n'est trouvé, une exception avec un code de
     *                                 statut 404 (Not Found) est levée.
     */
    @GetMapping("/scans/{id}")
    public Optional<Scan> getScan(@PathVariable Long id) {
        Optional<Scan> result = scanService.getScan(id);
        if (!result.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return result;
    }

    /**
     * Obtenir tous les fichiers scannés et stockés dans la table Fichier.
     * 
     * @return Iterable<Fichier> : informations des fichiers.
     */
    @GetMapping("/fichiers")
    public Iterable<Fichier> getFichiers() {
        return scanService.getFichiers();
    }

    /**
     * Obtenir tous les scans et les fichiers associés.
     * 
     * @return Iterable<ScanFichier> :
     *         informations des scans et des fichiers scannés lors du traitement du
     *         scan associé.
     */
    @GetMapping("/scans/infos/all")
    public Iterable<ScanFichier> getScansFichiers() {
        return scanService.getScansFichiers();
    }

    /**
     * Obtenir tous les fichiers scannés lors d'un scan en particulier.
     * 
     * @param id : identifiant du scan.
     * @return Iterable<ScanFichier> :
     *         informations du scan et des fichiers récupérés lors du scan.
     * @throws ResponseStatusException :
     *                                 Si aucun scan correspondant à l'identifiant
     *                                 n'est trouvé, une exception avec un code de
     *                                 statut 404 (Not Found) est levée.
     */
    @GetMapping("/scans/infos/{id}")
    public Iterable<ScanFichier> getInfosScan(@PathVariable Long id) {
        Iterable<ScanFichier> result = scanService.getInfosScan(id);
        Iterator<ScanFichier> iterator = result.iterator();
        if (!iterator.hasNext()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return result;
    }

    /**
     * Créer un nouveau scan et enregistrer les résultats dans la base de données.
     * 
     * @param scan : Scan à enregistrer (critères de sélection des fichiers
     *             scannés).
     * @return ResponseEntity<?> :
     *         - si le répertoire initial donné est valide, renvoie un
     *         ResponseEntity avec le scan créé et un code de statut 201 (Created).
     *         - sinon, renvoie un ResponseEntity avec un code d'erreur 400 (Bad
     *         Request).
     */
    @PostMapping("/scans")
    public ResponseEntity<?> newScan(@Valid @RequestBody Scan scan) {
        Scan newScan = scanService.saveResultatScan(scan);
        if (newScan == null) {
            return ResponseEntity.badRequest().body("Le répertoire initial donné est invalide.");
        } else {
            return new ResponseEntity<Scan>(newScan, HttpStatus.CREATED);
        }
    }

    /**
     * Dupliquer un scan en modifiant la valeur d'un champ (nombre de fichiers
     * maximum à scanner, profondeur maximale dans l'arborescence, filtres sur le
     * nom et le type de fichier).
     * 
     * @param id           : identifiant du scan à dupliquer.
     * @param nouvelleInfo : champ et nouvelle valeur du champ.
     * @return ResponseEntity<?> :
     *         - contient le scan dupliqué si l'opération réussit avec un code de
     *         statut 200 (OK).
     *         - si aucun scan correspondant à l'identifiant n'est trouvé, renvoie
     *         un ResponseEntity avec un code d'erreur 404 (Not Found).
     *         - si les nouvelles informations spécifiées sont invalides, renvoie un
     *         ResponseEntity avec un code d'erreur 400 (Bad Request) et un message
     *         d'erreur.
     */
    @PostMapping("/dupliquer/scan/{id}")
    public ResponseEntity<?> dupliquerScan(@PathVariable Long id,
            @Valid @RequestBody DupliquerScanChampDto nouvelleInfo) {
        try {
            Optional<Scan> result = scanService.dupliquerScan(id, nouvelleInfo.getChamp(), nouvelleInfo.getValeur());
            if (result.isPresent()) {
                return ResponseEntity.ok(result.get());
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    /**
     * Rejouer un scan.
     * 
     * @param id : identifiant du scan à rejouer.
     * @return Optional<Scan> : scan rejoué s'il est trouvé.
     * @throws ResponseStatusException :
     *                                 Si aucun scan correspondant à l'identifiant
     *                                 n'est trouvé, une exception avec un code de
     *                                 statut 404 (Not Found) est levée.
     */
    @PatchMapping("/rejouer/{id}")
    public Optional<Scan> rejouerScan(@PathVariable Long id) {
        Optional<Scan> result = scanService.rejouerScan(id);
        if (!result.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return result;
    }

    /**
     * Supprimer un scan.
     * 
     * @param id : identifiant du scan à supprimer.
     * @throws ResponseStatusException :
     *                                 Si aucun scan correspondant à l'identifiant
     *                                 n'est trouvé, une exception avec un code de
     *                                 statut 404 (Not Found) est levée.
     */
    @DeleteMapping("/scans/{id}")
    public void deleteScan(@PathVariable Long id) {
        boolean found = scanService.deleteScan(id);
        if (!found) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Comparer deux scans.
     * 
     * @param id1 : identifiant du premier scan.
     * @param id2 : identifiant du second scan.
     * @return String :
     *         informations sur chacun des deux scans et les fichiers
     *         associés (communs et spécifiques à chaque scan).
     */
    @GetMapping("/comparaison/{id1}/{id2}")
    public String comparaisonScans(@PathVariable Long id1, @PathVariable Long id2) {
        return comparaisonScansService.comparaison(id1, id2);
    }

    /**
     * Obtenir les statistiques d'un scan.
     * 
     * @param id : identifiant du scan.
     * @return String :
     *         statistiques du scan (moyenne des temps d'exécution par répertoire et
     *         par fichier).
     * @throws ResponseStatusException :
     *                                 Si aucun scan correspondant à l'identifiant
     *                                 n'est trouvé, une exception avec un code de
     *                                 statut 404 (Not Found) est levée avec un
     *                                 message d'erreur.
     */
    @GetMapping("/scans/statistiques/{id}")
    public String getStatistiquesScan(@PathVariable Long id) {
        String moyenne = scanService.getStatistiquesScan(id);
        if (moyenne == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Aucune statistique trouvée. Le scan n'existe pas.");
        }
        return moyenne;
    }

    /**
     * Obtenir les statistiques de l'ensemble des scans.
     * 
     * @return String :
     *         statistiques des scans (moyenne des temps d'exécution par répertoire
     *         et par fichier).
     * @throws ResponseStatusException :
     *                                 Si aucun scan n'est trouvé, une exception
     *                                 avec un code de statut 404 (Not Found) est
     *                                 levée avec un message d'erreur.
     */
    @GetMapping("/scans/statistiques/all")
    public String getStatistiques() {
        String moyenne = scanService.getStatistiques();
        if (moyenne == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Aucune statistique trouvée.");
        }
        return moyenne;
    }
}
