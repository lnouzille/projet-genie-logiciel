package fr.ensai.demo;

import fr.ensai.demo.service.ScanService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {
	@Autowired
	private static ScanService scanService;
	@Autowired
	@Qualifier("scanService")
	private static ScanService scanService2;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		if (DemoApplication.scanService == DemoApplication.scanService2) {
		}
	}

}