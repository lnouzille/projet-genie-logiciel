package fr.ensai.demo.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.model.ScanFichier;
import fr.ensai.demo.repository.ScanFichierRepository;

@Service
public class ComparaisonScansService {

    @Autowired
    private ScanFichierRepository scanFichierRepository;

    /**
     * Obtenir les informations sur le scan et les fichiers scannés associés.
     * 
     * @param idScan : identifiant du scan.
     * @return Pair<Scan, List<Fichier>> :
     *         informations sur le scan et les fichiers scannés associés.
     */
    public Pair<Scan, List<Fichier>> getInfos(Long idScan) {

        // Récupérer les informations en base de données
        Iterable<ScanFichier> infosScan = scanFichierRepository.findByScanIdScan(idScan);
        Iterator<ScanFichier> iterator = infosScan.iterator();
        if (!iterator.hasNext()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "L'un des scans que vous voulez comparer n'existe pas.");
        }

        // Stocker les résultats dans des objets Scan et List<Fichier>
        Scan scan = null;
        List<Fichier> fichiers = new ArrayList<Fichier>();
        for (ScanFichier sf : infosScan) {
            scan = sf.getScan();
            fichiers.add(sf.getFichier());
        }

        return Pair.of(scan, fichiers);
    }

    /**
     * Renvoyer la chaîne de caractères avec toutes les informations d'un scan.
     * 
     * @param scan : scan.
     * @return String : chaîne de caractères avec toutes les informations d'un scan.
     */
    public String getParametres(Scan scan) {
        String infosScan = "Scan n°" + scan.getIdScan() + "\n";
        infosScan += "---- Répertoire de départ : " + scan.getRepertoireInitial() + "\n";
        infosScan += "---- Système de fichier local : " + scan.isScanLocal() + "\n";
        infosScan += "---- Nombre max de fichiers : " + scan.getNbMaxFichiers() + "\n";
        infosScan += "---- Profondeur max : " + scan.getProfondeurMaxArbo() + "\n";
        infosScan += "---- Filtre sur le nom de fichier : " + scan.getFiltreNomFichier() + "\n";
        infosScan += "---- Filtre sur le type de fichier : " + scan.getFiltreTypeFichier() + "\n";
        infosScan += "---- Date du scan : " + scan.getDateScan() + "\n";
        infosScan += "---- Temps d'exécution : " + scan.getTempsExecution() + " ms \n";
        return infosScan;
    }

    /**
     * Obtenir la liste des fichiers communs à deux scans.
     * 
     * @param fichiers1 : liste des fichiers associés au scan 1.
     * @param fichiers2 : liste des fichiers associés au scan 2.
     * @return List<Fichier> : liste des fichiers communs.
     */
    public List<Fichier> getFichiersCommuns(List<Fichier> fichiers1, List<Fichier> fichiers2) {
        List<Fichier> fichiersCommuns = new ArrayList<>(fichiers1);
        fichiersCommuns.retainAll(fichiers2); // Retenir uniquement les éléments présents dans les deux listes
        return fichiersCommuns;
    }

    /**
     * Obtenir la liste des fichiers communs à deux scans sous la forme d'une chaîne
     * de caractères avec les informations sur le nom du fichier, la date de
     * modification et le répertoire contenant.
     * 
     * @param fichiers1 : liste des fichiers associés au scan 1.
     * @param fichiers2 : liste des fichiers associés au scan 2.
     * @return String : informations sur les fichiers communs à deux scans.
     */
    public String getFichiersCommunsText(List<Fichier> fichiers1, List<Fichier> fichiers2) {
        String fichiersText = "Fichiers communs \n";
        List<Fichier> fichiersCommuns = getFichiersCommuns(fichiers1, fichiers2); // Récupérer la liste des fichiers
                                                                                  // communs
        for (Fichier fichier : fichiersCommuns) {
            fichiersText += "---- " + fichier.getNomFichier() + " | " + fichier.getDateModification() + " | "
                    + fichier.getRepertoireContenant() + "\n";
        }
        return fichiersText;
    }

    /**
     * Obtenir la liste des fichiers spécifiques à un scan, non scannés par l'autre
     * scan.
     * 
     * @param scan            : scan.
     * @param fichiers        : liste des fichiers associés au scan.
     * @param fichiersCommuns : liste des fichiers communs aux deux scans.
     * @return String : informations sur les fichiers spécifiques au scan.
     */
    public String getFichiersRestants(Scan scan, List<Fichier> fichiers, List<Fichier> fichiersCommuns) {
        String fichiersText = "Autres fichiers spécifiques au scan n°" + scan.getIdScan() + "\n";
        List<Fichier> fichiersRestants = new ArrayList<>(fichiers);
        fichiersRestants.removeAll(fichiersCommuns); // Garder les fichiers spécifiques au scan.
        for (Fichier fichier : fichiersRestants) {
            fichiersText += "---- " + fichier.getNomFichier() + " | " + fichier.getDateModification() + " | "
                    + fichier.getRepertoireContenant() + "\n";
        }
        return fichiersText;
    }

    /**
     * Obtenir la chaîne de caractères permettant de comparer deux scans.
     * 
     * @param idScan1 : identifiant du scan 1.
     * @param idScan2 : identifiant du scan 2.
     * @return String : comparaison des deux scans.
     */
    public String comparaison(Long idScan1, Long idScan2) {

        // Récupération des informations du scan 1
        Pair<Scan, List<Fichier>> resultat1 = getInfos(idScan1);
        List<Fichier> fichiers1 = resultat1.getRight();
        Scan scan1 = resultat1.getLeft();

        // Récupération des informations du scan 2
        Pair<Scan, List<Fichier>> resultat2 = getInfos(idScan2);
        List<Fichier> fichiers2 = resultat2.getRight();
        Scan scan2 = resultat2.getLeft();

        String resultat = "Comparaison des scans n°" + scan1.getIdScan() + " et " + scan2.getIdScan() + "\n";

        // Récupérer les paramètres et les informations du scan
        resultat += getParametres(scan1) + getParametres(scan2);

        // Récupérer les fichiers scannés communs
        List<Fichier> fichiersCommuns = getFichiersCommuns(fichiers1, fichiers2);
        resultat += getFichiersCommunsText(fichiers1, fichiers2);

        // Récupérer les fichiers spécifiques à chaque scan (non communs)
        resultat += getFichiersRestants(scan1, fichiers1, fichiersCommuns);
        resultat += getFichiersRestants(scan2, fichiers2, fichiersCommuns);

        return resultat;
    }

}
