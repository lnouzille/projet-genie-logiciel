package fr.ensai.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ensai.demo.dto.ScanResultat;
import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.repository.FichierRepository;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.repository.ScanRepository;
import fr.ensai.demo.repository.StatistiquesScanRepository;
import fr.ensai.demo.service.traitementScan.AppliquerScanner;
import jakarta.transaction.Transactional;
import fr.ensai.demo.model.ScanFichier;
import fr.ensai.demo.model.StatistiquesScan;
import fr.ensai.demo.repository.ScanFichierRepository;

@Service
public class ScanService {

    @Autowired
    private ScanRepository scanRepository;
    @Autowired
    private FichierRepository fichierRepository;
    @Autowired
    private ScanFichierRepository scanFichierRepository;
    @Autowired
    private StatistiquesScanRepository statistiquesScanRepository;

    /**
     * Obtenir les informations de tous les scans stockés dans la table Scan.
     * 
     * @return Iterable<Scan> : informations des scans.
     */
    public Iterable<Scan> getScans() {
        return scanRepository.findAll();
    }

    /**
     * Obtenir les informations d'un scan de la table Scan.
     * 
     * @param id : identifiant du scan.
     * @return Optional<Scan> : informations du scan s'il est présent dans la table.
     */
    public Optional<Scan> getScan(final Long id) {
        return scanRepository.findById(id);
    }

    /**
     * Obtenir tous les fichiers scannés et stockés dans la table Fichier.
     * 
     * @return Iterable<Fichier> : informations des fichiers.
     */
    public Iterable<Fichier> getFichiers() {
        return fichierRepository.findAll();
    }

    /**
     * Obtenir tous les scans et les fichiers associés.
     * 
     * @return Iterable<ScanFichier> :
     *         informations des scans et des fichiers scannés lors du traitement du
     *         scan associé.
     */
    public Iterable<ScanFichier> getScansFichiers() {
        return scanFichierRepository.findAll();
    }

    /**
     * Obtenir tous les fichiers scannés lors d'un scan en particulier.
     * 
     * @param id : identifiant du scan.
     * @return Iterable<ScanFichier> :
     *         informations du scan et des fichiers récupérés lors du scan.
     */
    public Iterable<ScanFichier> getInfosScan(final Long id) {
        return scanFichierRepository.findByScanIdScan(id);
    }

    /**
     * Dupliquer un scan en modifiant la valeur d'un champ.
     * 
     * @param id     : identifiant du scan à dupliquer.
     * @param champ  : champ à modifier.
     * @param valeur : nouvelle valeur pour le champ.
     * @return Optional<Scan> :
     *         scan dupliqué avec le champ mis à jour (si le scan à dupliquer
     *         existait).
     */
    public Optional<Scan> dupliquerScan(final Long id, String champ, String valeur) {

        // Récupérer le scan
        Optional<Scan> optionalScan = getScan(id);

        if (optionalScan.isPresent()) {

            // Dupliquer le scan pour ne pas l'écraser lors de l'enregistrement
            Scan scan = optionalScan.get();
            Scan scanDuplique = scan.duplicate();

            // Vérifier que le champ spécifier est valide et le mettre à jour
            switch (champ) {
                case "nbMaxFichiers" -> scanDuplique.setNbMaxFichiers(Integer.parseInt(valeur));
                case "profondeurMaxArbo" -> scanDuplique.setProfondeurMaxArbo(Integer.parseInt(valeur));
                case "filtreNomFichier" -> scanDuplique.setFiltreNomFichier(valeur);
                case "filtreTypeFichier" -> scanDuplique.setFiltreTypeFichier(valeur);
                default -> throw new IllegalArgumentException("Champ invalide : " + champ);
            }

            // Lancer le traitement du nouveau scan et l'enregistrer
            Scan savedScan = saveResultatScan(scanDuplique);
            optionalScan = Optional.ofNullable(savedScan);
        }

        return optionalScan;
    }

    /**
     * Rejouer un scan.
     * 
     * @param id : identifiant du scan à rejouer.
     * @return Optional<Scan> : scan rejoué s'il est trouvé.
     */
    @Transactional
    public Optional<Scan> rejouerScan(final long id) {

        // Récupérer le scan
        Optional<Scan> optionalScan = getScan(id);

        if (optionalScan.isPresent()) {
            Scan scan = optionalScan.get();
            // Supprimer toutes les lignes de ScanFichier associées au scan
            scanFichierRepository.deleteByScanIdScan(id);
            // Supprimer ses statistiques
            statistiquesScanRepository.deleteByScanIdScan(id);
            // Relancer le scan et l'enregistrer
            Scan savedScan = saveResultatScan(scan);
            optionalScan = Optional.ofNullable(savedScan);
        }

        return optionalScan;
    }

    /**
     * Créer un nouveau scan et enregistrer les résultats dans la base de données.
     * 
     * @param scan : scan à enregistrer.
     * @return Scan : scan créé.
     */
    @Transactional
    public Scan saveResultatScan(Scan scan) {

        // Récupérer les fichiers scannés
        AppliquerScanner scanner = new AppliquerScanner();
        ScanResultat resultat = scanner.getResultatScan(scan);
        if (resultat == null) {
            return null;
        }
        List<Fichier> fichiers = resultat.getfichiers();
        Scan updatedScan = resultat.getScan();
        StatistiquesScan statistiques = resultat.getStatistiques();

        // Enregistrer le nouveau scan dans la table des scans
        Scan savedScan = scanRepository.save(updatedScan);

        // Enregistrer ses statistiques
        statistiquesScanRepository.save(statistiques);

        // Enregistrer les fichiers récupérés dans la table des fichiers
        for (Fichier fichier : fichiers) {

            // Vérifier si le fichier existe déjà
            Fichier existingFichier = fichierRepository.findByNomFichierAndRepertoireContenantAndDateModification(
                    fichier.getNomFichier(),
                    fichier.getRepertoireContenant(),
                    fichier.getDateModification());

            // Si le fichier n'existe pas, l'enregistrer dans la table des fichiers
            if (existingFichier == null) {
                existingFichier = fichierRepository.save(fichier);
            }

            // Enregistrer une ligne dans la table ScanFichier pour lier le fichier au scan
            ScanFichier scanFichier = new ScanFichier(savedScan, existingFichier);
            scanFichierRepository.save(scanFichier);
        }

        return savedScan;
    }

    /**
     * Supprimer un scan.
     * 
     * @param id : identifiant du scan à supprimer.
     * @return boolean : true si le scan existait bien et false sinon.
     */
    @Transactional
    public boolean deleteScan(final Long id) {

        Optional<Scan> scan = getScan(id);

        // Supprimer toutes les lignes de ScanFichier associées au scan
        scanFichierRepository.deleteByScanIdScan(id);

        // Supprimer les statistiques du scan
        statistiquesScanRepository.deleteByScanIdScan(id);

        // Supprimer le scan lui-même
        scanRepository.deleteById(id);

        return scan.isPresent();
    }

    /**
     * Renvoyer la chaîne de caractères avec les informations sur les moyennes des
     * temps d'exécution par répertoire et par fichier.
     * 
     * @param statistiques : statistiques des scans.
     * @return String :
     *         informations sur les moyennes des temps d'exécution par répertoire et
     *         par fichier.
     */
    public String getMoyenneTempsExecution(List<StatistiquesScan> statistiques) {

        // Calcul de la moyenne des temps d'exécution par répertoire
        double sommeTempsExecutionTotalRepertoires = statistiques.stream()
                .mapToDouble(stat -> (double) stat.getTempsExecutionTotalRepertoires()).sum();
        double sommeNbRepertoiresScannes = statistiques.stream()
                .mapToDouble(stat -> (double) stat.getNbRepertoiresScannes()).sum();
        double moyenneRepertoires = sommeTempsExecutionTotalRepertoires / sommeNbRepertoiresScannes;

        // Calcul de la moyenne des temps d'exécution par fichier
        double sommeTempsExecutionTotalFichiers = statistiques.stream()
                .mapToDouble(stat -> (double) stat.getTempsExecutionTotalFichiers()).sum();
        double sommeNbFichiersScannes = statistiques.stream()
                .mapToDouble(stat -> (double) stat.getNbFichiersScannes()).sum();
        double moyenneFichiers = sommeTempsExecutionTotalFichiers / sommeNbFichiersScannes;

        // Affichage sous forme d'une chaîne de caractères
        String moyenne = "";
        if (!Double.isNaN(moyenneRepertoires) && !Double.isNaN(moyenneFichiers)) {
            moyenne = "Moyenne temps d'exécution par répertoire: "
                    + String.format("%.2f", moyenneRepertoires) + " ms \n";
            moyenne += "Moyenne temps d'exécution par fichier: "
                    + String.format("%.2f", moyenneFichiers) + " ms \n";
            return moyenne;
        } else {
            return null;
        }
    }

    /**
     * Obtenir les statistiques d'un scan.
     * 
     * @param id : identifiant du scan.
     * @return String :
     *         statistiques du scan (moyenne des temps d'exécution par répertoire et
     *         par fichier).
     */
    @Transactional
    public String getStatistiquesScan(final Long id) {

        // Récupérer les statistiques du scan
        Iterable<StatistiquesScan> statistiques = statistiquesScanRepository.findByScanIdScan(id);
        List<StatistiquesScan> statistiquesList = (List<StatistiquesScan>) statistiques;

        return getMoyenneTempsExecution(statistiquesList);
    }

    /**
     * Obtenir les statistiques de l'ensemble des scans.
     * 
     * @return String :
     *         statistiques des scans (moyenne des temps d'exécution par répertoire
     *         et par fichier).
     */
    public String getStatistiques() {

        // Récupérer les statistiques des scans
        Iterable<StatistiquesScan> statistiques = statistiquesScanRepository.findAll();
        List<StatistiquesScan> statistiquesList = (List<StatistiquesScan>) statistiques;

        return getMoyenneTempsExecution(statistiquesList);
    }
}
