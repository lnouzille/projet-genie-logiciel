package fr.ensai.demo.service.traitementScan.composite;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.service.traitementScan.observer.Observer;
import fr.ensai.demo.service.traitementScan.observer.TempsExecutionObserver;

public class DossierComposite implements Composant {

    private File file;
    private int profondeurArbo;
    private boolean canAdd;
    private Long tempsExecution;
    private List<Fichier> fichiersCommuns;
    private List<Composant> composants = new ArrayList<Composant>();
    private List<Observer> observers = new ArrayList<Observer>();

    /**
     * Constructeur.
     * 
     * @param file            : dossier sous la forme d'un objet File.
     * @param fichiersCommuns : liste des fichiers collectés lors du traitement du
     *                        scan.
     */
    public DossierComposite(File file, List<Fichier> fichiersCommuns) {
        this.file = file;
        this.fichiersCommuns = fichiersCommuns;
        this.canAdd = true;
    }

    /**
     * Collecter les informations sur le Composant et en notifier les observateurs.
     */
    @Override
    public void traiter() {

        long debutTraitement = System.currentTimeMillis();

        if (composants.isEmpty()) {
            explorerSousRepertoires();
        }

        // Récolter les informations sur chacun des composants du DossierComposite
        for (Composant c : composants) {
            c.traiter();
        }

        long finTraitement = System.currentTimeMillis();

        // Calculer le temps d'exécution
        tempsExecution = finTraitement - debutTraitement;

        // Informer les observateurs
        notifyObservers(this);
    }

    /**
     * Explorer les sous-répertoires du répertoire actuel.
     * Pour chaque sous-répertoire trouvé, créer un objet DossierComposite et
     * l'ajouter à la liste des composants.
     * Pour chaque fichier trouvé, créer un objet FichierComposant et l'ajouter à la
     * liste des composants.
     * Cette méthode effectue une exploration récursive des sous-répertoires.
     */
    public void explorerSousRepertoires() {

        // Lister les fichiers et répertoires contenus dans le répertoire actuel
        File[] files = this.file.listFiles();

        if (files != null) {

            // Parcourir tous les fichiers et sous-répertoires
            for (File childFile : files) {

                if (childFile.isDirectory()) { // Sous répertoire

                    // Créer un DossierComposite pour le sous-répertoire
                    DossierComposite sousDossier = new DossierComposite(childFile, fichiersCommuns);

                    // Définir la profondeur du sous-répertoire dans l'arborescence
                    sousDossier.setProfondeurArbo(this.getProfondeurArbo() + 1);

                    // Ajouter tous les observateurs pour le nouveau sous-répertoire à explorer
                    addAllObserver(sousDossier);

                    // Appel récursif pour explorer les sous-répertoires
                    sousDossier.explorerSousRepertoires();

                    // Ajouter le sous-répertoire à la liste des composants
                    composants.add(sousDossier);

                } else { // Fichier

                    // Créer un FichierComposant pour le fichier
                    FichierComposant fichier = new FichierComposant(childFile, fichiersCommuns);

                    // Définir la profondeur du fichier dans l'arborescence
                    fichier.setProfondeurArbo(this.getProfondeurArbo() + 1);

                    // Ajouter tous les observateurs pour le nouveau fichier
                    addAllObserver(fichier);

                    // Ajouter le fichier à la liste des composants
                    composants.add(fichier);
                }
            }
        }
    }

    /**
     * Ajouter un Composant à la liste des composants.
     * 
     * @param c : Composant à ajouter.
     */
    public void add(Composant c) {
        composants.add(c);
    }

    /**
     * Supprimer un Composant de la liste des composants.
     * 
     * @param c : Composant à supprimer.
     */
    public void remove(Composant c) {
        composants.remove(c);
    }

    /**
     * Récupérer toute la liste des composants.
     * 
     * @return List<Composant> : liste des composants.
     */
    public List<Composant> getChildren() {
        return composants;
    }

    /**
     * Ajouter un observateur.
     * 
     * @param o : observateur à ajouter.
     */
    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    /**
     * Ajouter tous les observeurs de l'observé actuel à un nouveau observé.
     * 
     * @param c : composant auquel ajouter tous les observateurs.
     */
    public void addAllObserver(Composant c) {
        for (Observer o : observers) {
            c.addObserver(o);
        }
    }

    /**
     * Supprimer un observateur.
     * 
     * @param o : observateur à supprimer.
     */
    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    /**
     * Informer les observateurs en appelant leur méthode de mise à jour avec le
     * composant spécifié.
     * 
     * @param c : composant dont l'état a changé.
     */
    @Override
    public void notifyObservers(Composant c) {
        for (Observer o : observers) {
            o.update(c);
        }
    }

    /**
     * Ajouter un fichier à la liste des fichiers scannés s'il satisfait tous les
     * critères.
     * 
     * @param fichier : fichier à ajouter.
     */
    @Override
    public void addInfosFichier(Fichier fichier) {

        // Vérifier si les observateurs autorisent à ajouter le fichier
        for (Observer o : observers) {
            if (!(o instanceof TempsExecutionObserver)) {
                boolean reponse = o.getCanAdd();
                if (this.canAdd && !reponse) {
                    canAdd = false;
                    break; // Dès qu'un observateur n'autorise pas l'ajout du fichier, on arrête la boucle
                }
            }
        }

        // Ajouter le fichier si c'est le cas
        if (canAdd) {
            fichiersCommuns.add(fichier);
        }
    }

    /**
     * Obtenir la profondeur du Composant dans l'arborescence.
     * 
     * @return int : profondeur du Composant dans l'arborescence.
     */
    @Override
    public int getProfondeurArbo() {
        return this.profondeurArbo;
    }

    /**
     * Définir la profondeur du Composant dans l'arborescence.
     * 
     * @param profondeurArbo : profondeur du Composant dans l'arborescence.
     */
    @Override
    public void setProfondeurArbo(int profondeurArbo) {
        this.profondeurArbo = profondeurArbo;
    }

    /**
     * Récupérer la liste des fichiers scannés.
     * 
     * @return List<Fichier> : liste des fichiers scannés.
     */
    @Override
    public List<Fichier> getInfosFichier() {
        return this.fichiersCommuns;
    }

    /**
     * Récupérer le booléen qui sert à définir si le fichier peut être ajouté à la
     * liste des fichiers scannés (s'il correspond bien aux critères).
     * 
     * @return boolean : true si le fichier peut être ajouté et false sinon.
     */
    @Override
    public boolean getCanAdd() {
        return this.canAdd;
    }

    /**
     * Obtenir le temps d'exécution du Composant.
     * 
     * @return Long : temps d'exécution du Composant.
     */
    @Override
    public Long getTempsExecution() {
        return this.tempsExecution;
    }
}
