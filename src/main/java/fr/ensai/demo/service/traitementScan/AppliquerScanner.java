package fr.ensai.demo.service.traitementScan;

import java.util.Date;
import java.util.List;

import fr.ensai.demo.dto.ScanResultat;
import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.model.StatistiquesScan;
import fr.ensai.demo.service.traitementScan.composite.Composant;
import fr.ensai.demo.service.traitementScan.observer.FiltreNomFichierObserver;
import fr.ensai.demo.service.traitementScan.observer.FiltreTypeFichierObserver;
import fr.ensai.demo.service.traitementScan.observer.MaxFichiersObserver;
import fr.ensai.demo.service.traitementScan.observer.Observer;
import fr.ensai.demo.service.traitementScan.observer.ProfondeurArboObserver;
import fr.ensai.demo.service.traitementScan.observer.TempsExecutionObserver;
import fr.ensai.demo.service.traitementScan.strategie.StrategieAWS;
import fr.ensai.demo.service.traitementScan.strategie.StrategieLocal;
import fr.ensai.demo.service.traitementScan.strategie.ContexteStrategie;

public class AppliquerScanner {

    /**
     * Retourner un objet de type ScanResultat qui contient toutes les informations
     * sur le scan (critères renseignés, statistiques) et la liste des fichiers
     * scannés correspondant aux critères définis.
     * 
     * @param scan : informations sur le scan.
     * @return ScanResultat : résultat du scan.
     */
    public ScanResultat getResultatScan(Scan scan) {

        // Lancement de la mesure du temps d'exécution du scan
        Long startTime = System.currentTimeMillis();

        // Définir la stratégie utilisée (local ou S3)
        ContexteStrategie contexteStrategie = new ContexteStrategie();
        if (scan.isScanLocal()) {
            contexteStrategie.setStrategie(new StrategieLocal());
        } else {
            contexteStrategie.setStrategie(new StrategieAWS());
        }

        // Récupération du Composant racine en fonction de la statégie
        Composant racine = contexteStrategie.executerStrategie(scan);
        if (racine == null) {
            return null;
        }

        // Création et ajout des observers
        if (scan.getNbMaxFichiers() != 0) {
            Observer maxFichiersObserver = new MaxFichiersObserver(scan.getNbMaxFichiers());
            racine.addObserver(maxFichiersObserver);
        }
        if (scan.getProfondeurMaxArbo() != 0) {
            Observer profondeurArboObserver = new ProfondeurArboObserver(scan.getProfondeurMaxArbo());
            racine.addObserver(profondeurArboObserver);
        }
        if (!scan.getFiltreNomFichier().equals("string") && !scan.getFiltreNomFichier().equals("aucun")) {
            Observer filtreNomFichierObserver = new FiltreNomFichierObserver(scan.getFiltreNomFichier());
            racine.addObserver(filtreNomFichierObserver);
        } else {
            scan.setFiltreNomFichier("aucun");
        }
        if (!scan.getFiltreTypeFichier().equals("string") && !scan.getFiltreTypeFichier().equals("aucun")) {
            Observer filtreTypeFichierObserver = new FiltreTypeFichierObserver(scan.getFiltreTypeFichier());
            racine.addObserver(filtreTypeFichierObserver);
        } else {
            scan.setFiltreTypeFichier("aucun");
        }
        Observer tempsExecutionObserver = new TempsExecutionObserver();
        racine.addObserver(tempsExecutionObserver);

        // Exploration
        racine.traiter();
        Long stopTime = System.currentTimeMillis();
        Long tempsExecution = stopTime - startTime;

        // Mise à jour des variables de Scan
        scan.setDateScan(new Date());
        scan.setTempsExecution(tempsExecution);

        // Création des variables de StatistiquesScans
        StatistiquesScan statistiquesScan = new StatistiquesScan();
        statistiquesScan.setScan(scan);
        statistiquesScan.setTempsExecutionTotalRepertoires(
                ((TempsExecutionObserver) tempsExecutionObserver).getTempsExecutionTotalRepertoires());
        statistiquesScan
                .setNbRepertoiresScannes(((TempsExecutionObserver) tempsExecutionObserver).getNbRepertoires());
        statistiquesScan.setTempsExecutionTotalFichiers(
                ((TempsExecutionObserver) tempsExecutionObserver).getTempsExecutionTotalFichiers());
        statistiquesScan.setNbFichiersScannes(((TempsExecutionObserver) tempsExecutionObserver).getNbFichiers());

        // Récupération des fichiers correspondant aux différentes contraintes
        List<Fichier> infos = racine.getInfosFichier();

        return new ScanResultat(scan, infos, statistiquesScan);
    }
}
