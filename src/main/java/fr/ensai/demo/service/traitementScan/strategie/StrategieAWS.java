package fr.ensai.demo.service.traitementScan.strategie;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.service.traitementScan.composite.Composant;
import fr.ensai.demo.service.traitementScan.composite.S3Composant;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.HeadBucketResponse;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;

public class StrategieAWS implements Strategie {

    Logger LOG = LoggerFactory.getLogger(StrategieAWS.class);

    /**
     * Récupérer le Composant initialisé.
     * 
     * @param scan : informations sur le scan.
     * @return Composant :
     *         composant initialisé et adapté au système de fichier AWS S3.
     */
    @Override
    public Composant getComposant(Scan scan) {

        // Initialisation de la liste des fichiers récupérés lors du scan
        List<Fichier> fichiersCommuns = new ArrayList<>();

        // Initialisation du dossier racine à explorer
        String nomBucket = scan.getRepertoireInitial();

        try { // Vérifier qu'il existe

            S3Client s3 = createS3Client();
            HeadBucketRequest headBucketRequest = HeadBucketRequest.builder().bucket(nomBucket).build();
            HeadBucketResponse headBucketResponse = s3.headBucket(headBucketRequest);
            LOG.info("Réponse sur l'existence du bucket S3 : " + headBucketResponse);
            // Le bucket existe, créer et retourner le composant S3 racine
            Composant racine = new S3Composant(nomBucket, fichiersCommuns);
            return racine;

        } catch (NoSuchBucketException e) { // Sinon , retourner null
            return null;
        }
    }

    /**
     * Créer un client S3.
     * Existe uniquement pour faciliter les tests.
     * 
     * @return S3Client : client S3.
     */
    protected S3Client createS3Client() {
        return S3Client.create();
    }
}
