package fr.ensai.demo.service.traitementScan.strategie;

import fr.ensai.demo.model.Scan;
import fr.ensai.demo.service.traitementScan.composite.Composant;

public interface Strategie {

    /**
     * Récupérer le Composant initialisé.
     * 
     * @param scan : informations sur le scan.
     * @return Composant : Composant initialisé.
     */
    public Composant getComposant(Scan scan);

}
