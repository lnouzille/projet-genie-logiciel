package fr.ensai.demo.service.traitementScan.strategie;

import fr.ensai.demo.model.Scan;
import fr.ensai.demo.service.traitementScan.composite.Composant;

public class ContexteStrategie {

    private Strategie strategie;

    /**
     * Définir la stratégie à utiliser pour instancier un composant (S3 ou local).
     * 
     * @param s : stratégie à utiliser.
     */
    public void setStrategie(Strategie s) {
        this.strategie = s;
    }

    /**
     * Récupérer le composant initialisé et adapté au système de fichiers en
     * fonction de la stratégie choisie.
     * 
     * @param scan : informations sur le scan.
     * @return Composant :
     *         composant initialisé et adapté au système de fichiers
     */
    public Composant executerStrategie(Scan scan) {
        return strategie.getComposant(scan);
    }

}
