package fr.ensai.demo.service.traitementScan.observer;

import fr.ensai.demo.service.traitementScan.composite.Composant;

public interface Observer {

    /**
     * Mettre à jour de l'état du système en conséquence du changement d'état de
     * l'observé (Composant).
     * 
     * @param c : composant dont l'état a changé.
     */
    void update(Composant c);

    /**
     * Récupérer le booléen qui sert à définir si le fichier peut être ajouté à la
     * liste des fichiers scannés en fonction du critère traité par l'observateur.
     * 
     * @return boolean : true si le fichier peut être ajouté et false sinon.
     */
    boolean getCanAdd();

}