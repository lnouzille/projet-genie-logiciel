package fr.ensai.demo.service.traitementScan.observer;

import fr.ensai.demo.service.traitementScan.composite.Composant;

public class ProfondeurArboObserver implements Observer {

    private int profondeurMaxArbo;
    private boolean canAdd;

    /**
     * Constructeur.
     * 
     * @param profondeurMaxArbo : profondeur maximale dans l'arborescence.
     */
    public ProfondeurArboObserver(int profondeurMaxArbo) {
        this.profondeurMaxArbo = profondeurMaxArbo;
    }

    /**
     * Mettre à jour la variable canAdd de l'observateur.
     * - Si la profondeur dans l'arborescence du Composant est inférieure ou égale à
     * la profondeur maximale, elle est mise à jour à True.
     * - Sinon, elle est mise à jour à False.
     * 
     * @param c : composant dont l'état a changé.
     */
    @Override
    public void update(Composant c) {
        if (c.getProfondeurArbo() > profondeurMaxArbo) {
            canAdd = false;
        } else {
            canAdd = true;
        }
    }

    /**
     * Récupérer le booléen qui sert à définir si le fichier peut être ajouté à la
     * liste des fichiers scannés en fonction du critère traité par l'observateur.
     * 
     * @return boolean : true si le fichier peut être ajouté et false sinon.
     */
    @Override
    public boolean getCanAdd() {
        return this.canAdd;
    }
}
