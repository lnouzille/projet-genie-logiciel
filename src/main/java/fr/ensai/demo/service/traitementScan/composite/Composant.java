package fr.ensai.demo.service.traitementScan.composite;

import java.util.List;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.service.traitementScan.observer.Observer;

public interface Composant {

    /**
     * Collecter les informations sur le Composant et en notifier les observateurs.
     */
    void traiter();

    /**
     * Ajouter un observateur.
     * 
     * @param o : observateur à ajouter.
     */
    void addObserver(Observer o);

    /**
     * Supprimer un observateur.
     * 
     * @param o : observateur à supprimer.
     */
    void removeObserver(Observer o);

    /**
     * Informer les observateurs en appelant leur méthode de mise à jour avec le
     * composant spécifié.
     * 
     * @param c : composant dont l'état a changé.
     */
    void notifyObservers(Composant c);

    /**
     * Ajouter un fichier à la liste des fichiers scannés s'il satisfait tous les
     * critères.
     * 
     * @param fichier : fichier à ajouter.
     */
    void addInfosFichier(Fichier fichier);

    /**
     * Récupérer la liste des fichiers scannés.
     * 
     * @return List<Fichier> : liste des fichiers scannés.
     */
    List<Fichier> getInfosFichier();

    /**
     * Obtenir la profondeur du Composant dans l'arborescence.
     * 
     * @return int : profondeur du Composant dans l'arborescence.
     */
    int getProfondeurArbo();

    /**
     * Définir la profondeur du Composant dans l'arborescence.
     * 
     * @param profondeurArbo : profondeur du Composant dans l'arborescence.
     */
    void setProfondeurArbo(int profondeurArbo);

    /**
     * Récupérer le booléen qui sert à définir si le fichier peut être ajouté à la
     * liste des fichiers scannés (s'il correspond bien aux critères).
     * 
     * @return boolean : true si le fichier peut être ajouté et false sinon.
     */
    boolean getCanAdd();

    /**
     * Obtenir le temps d'exécution du Composant.
     * 
     * @return Long : temps d'exécution du Composant.
     */
    Long getTempsExecution();

}
