package fr.ensai.demo.service.traitementScan.composite;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.service.traitementScan.observer.Observer;
import fr.ensai.demo.service.traitementScan.observer.TempsExecutionObserver;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.S3Object;

public class S3Composant implements Composant {

    private int profondeurArbo;
    private boolean canAdd;
    private Long tempsExecution;
    private List<Fichier> fichiersCommuns;
    private Fichier infosFichier;
    private List<Observer> observers = new ArrayList<Observer>();
    private String bucketName;
    private S3Client s3Client;

    /**
     * Constructeur.
     * 
     * @param bucketName      : nom du bucket S3.
     * @param fichiersCommuns : liste des fichiers collectés lors du traitement du
     *                        scan.
     */
    public S3Composant(String bucketName, List<Fichier> fichiersCommuns) {
        this.bucketName = bucketName;
        this.fichiersCommuns = fichiersCommuns;
        this.s3Client = createS3Client();
        this.canAdd = true;
    }

    /**
     * Collecter les informations sur le Composant et en notifier les observateurs.
     */
    @Override
    public void traiter() {

        // Requête pour lister les objets dans le bucket
        ListObjectsV2Request listObjectsRequest = ListObjectsV2Request.builder()
                .bucket(bucketName)
                .build();

        // Exécution de la requête
        ListObjectsV2Response listObjectsResponse = s3Client.listObjectsV2(listObjectsRequest);

        // Récupération de la liste des objets dans le bucket
        List<S3Object> objectSummaries = listObjectsResponse.contents();

        // Parcours de la liste des objets
        for (S3Object objectSummary : objectSummaries) {

            long debutTraitement = System.currentTimeMillis();

            // Récolter les informations sur les fichiers et les stocker dans un objet
            // Fichier
            infosFichier = new Fichier();
            String key = objectSummary.key(); // Chemin absolu
            String nomFichier = key.substring(key.lastIndexOf('/') + 1); // Nom du fichier
            infosFichier.setNomFichier(nomFichier);
            Instant lastModified = objectSummary.lastModified(); // Date de modification
            infosFichier.setDateModification(Date.from(lastModified));
            infosFichier.setPoids(objectSummary.size()); // Poids
            String directory; // Répertoire contenant
            if(key.lastIndexOf('/') != -1) {
                directory = key.substring(0, key.lastIndexOf('/'));
            } else {
                directory = key;
            }
            // String directory = key.substring(0, key.lastIndexOf('/')); 
            infosFichier.setRepertoireContenant(directory);
            String typeFichier = getFileType(nomFichier); // Type de fichier
            infosFichier.setTypeFichier(typeFichier);
            int profondeur = directory.split("/").length + 1; // Profondeur du fichier dans l'arborescence
            this.setProfondeurArbo(profondeur);

            long finTraitement = System.currentTimeMillis();

            // Calculer le temps d'exécution
            tempsExecution = finTraitement - debutTraitement;

            // Informer les observateurs
            notifyObservers(this);

            // Ajouter le fichier à la liste des fichiers scannés s'il répond aux critères
            addInfosFichier(infosFichier);

            // Réinitialiser le booléen sur l'ajout du fichier pour le prochain fichier
            // traité
            canAdd = true;
        }

        s3Client.close(); // Fermeture du client S3
    }

    /**
     * Récupérer le type de fichier.
     * 
     * @param nomFichier : nom du fichier.
     * @return String : type de fichier.
     */
    public String getFileType(String nomFichier) {
        int dotIndex = nomFichier.lastIndexOf('.');
        if (dotIndex != -1) {
            return nomFichier.substring(dotIndex + 1);
        } else {
            return "Unknown";
        }
    }

    /**
     * Ajouter un fichier à la liste des fichiers scannés s'il satisfait tous les
     * critères.
     * 
     * @param fichier : fichier à ajouter.
     */
    @Override
    public void addInfosFichier(Fichier fichier) {

        // Vérifier si les observateurs autorisent à ajouter le fichier
        for (Observer o : observers) {
            if (!(o instanceof TempsExecutionObserver)) {
                boolean reponse = o.getCanAdd();
                if (this.canAdd && !reponse) {
                    canAdd = false;
                    break; // Dès qu'un observateur n'autorise pas l'ajout du fichier, on arrête la boucle
                }
            }
        }

        // Ajouter le fichier si c'est le cas
        if (canAdd) {
            fichiersCommuns.add(fichier);
        }
    }

    /**
     * Récupérer les informations d'un fichier via l'objet Fichier.
     * 
     * @return Fichier : informations du fichier.
     */
    public Fichier getInfos() {
        return this.infosFichier;
    }

    /**
     * Ajouter un observateur.
     * 
     * @param o : observateur à ajouter.
     */
    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    /**
     * Supprimer un observateur.
     * 
     * @param o : observateur à supprimer.
     */
    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    /**
     * Informer les observateurs en appelant leur méthode de mise à jour avec le
     * composant spécifié.
     * 
     * @param c : composant dont l'état a changé.
     */
    @Override
    public void notifyObservers(Composant c) {
        for (Observer o : observers) {
            o.update(c);
        }
    }

    /**
     * Obtenir la profondeur du Composant dans l'arborescence.
     * 
     * @return int : profondeur du Composant dans l'arborescence.
     */
    @Override
    public int getProfondeurArbo() {
        return this.profondeurArbo;
    }

    /**
     * Définir la profondeur du Composant dans l'arborescence.
     * 
     * @param profondeurArbo : profondeur du Composant dans l'arborescence.
     */
    @Override
    public void setProfondeurArbo(int profondeurArbo) {
        this.profondeurArbo = profondeurArbo;
    }

    /**
     * Récupérer la liste des fichiers scannés.
     * 
     * @return List<Fichier> : liste des fichiers scannés.
     */
    @Override
    public List<Fichier> getInfosFichier() {
        return this.fichiersCommuns;
    }

    /**
     * Récupérer le booléen qui sert à définir si le fichier peut être ajouté à la
     * liste des fichiers scannés (s'il correspond bien aux critères).
     * 
     * @return boolean : true si le fichier peut être ajouté et false sinon.
     */
    @Override
    public boolean getCanAdd() {
        return this.canAdd;
    }

    /**
     * Obtenir le temps d'exécution du Composant.
     * 
     * @return Long : temps d'exécution du Composant.
     */
    @Override
    public Long getTempsExecution() {
        return this.tempsExecution;
    }

    /**
     * Créer un client S3.
     * Existe uniquement pour faciliter les tests.
     * 
     * @return S3Client : client S3.
     */
    protected S3Client createS3Client() {
        return S3Client.builder().region(Region.US_EAST_1).build();
    }
}
