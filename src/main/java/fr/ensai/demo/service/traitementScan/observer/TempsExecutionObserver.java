package fr.ensai.demo.service.traitementScan.observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.service.traitementScan.composite.Composant;
import fr.ensai.demo.service.traitementScan.composite.DossierComposite;
import fr.ensai.demo.service.traitementScan.composite.FichierComposant;
import fr.ensai.demo.service.traitementScan.composite.S3Composant;

public class TempsExecutionObserver implements Observer {

    private Long tempsExecutionTotalRepertoires;
    private Long nbRepertoires;
    private Long tempsExecutionTotalFichiers;
    private Long nbFichiers;
    private Map<String, Long> mapRepertoiresS3 = new HashMap<>();

    /**
     * Constructeur.
     */
    public TempsExecutionObserver() {
        this.tempsExecutionTotalRepertoires = Long.valueOf(0);
        this.nbRepertoires = Long.valueOf(0);
        this.tempsExecutionTotalFichiers = Long.valueOf(0);
        this.nbFichiers = Long.valueOf(0);
    }

    /**
     * Mettre à jour les variables des temps d'exécution et des nombres de
     * répertoires et de fichiers scannés en fonction du type de Composant.
     * 
     * @param c : composant dont l'état a changé.
     */
    @Override
    public void update(Composant c) {

        if (c instanceof FichierComposant) { // FichierComposant
            tempsExecutionTotalFichiers += ((FichierComposant) c).getTempsExecution();
            nbFichiers++;

        } else if (c instanceof DossierComposite) { // DossierComposite
            tempsExecutionTotalRepertoires += ((DossierComposite) c).getTempsExecution();
            nbRepertoires++;

        } else { // S3Composant

            // Pour les fichiers
            tempsExecutionTotalFichiers += ((S3Composant) c).getTempsExecution();
            nbFichiers++;

            // Pour les dossiers

            // Récupérer le temps d'exécution du composant
            Long tempsExecutionComposant = ((S3Composant) c).getTempsExecution();
            // Récupérer le répertoire contenant du composant
            Fichier fichier = ((S3Composant) c).getInfos();
            String repertoire = fichier.getRepertoireContenant();

            // Parcourir tous les éléments du dictionnaire pour obtenir le temps d'exécution
            // total de chaque répertoire en fonction du temps d'exécution du composant
            Iterator<Map.Entry<String, Long>> iterator = mapRepertoiresS3.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, Long> entry = iterator.next();
                String cle = entry.getKey();
                Long valeur = entry.getValue();
                // Si le répertoire contenant du S3Composant est égale ou est contenu dans un
                // répertoire, ajouter son temps d'exécution au temps d'exécution total de ce
                // répertoire
                if (repertoire.equals(cle) || repertoire.contains(cle)) {
                    mapRepertoiresS3.put(cle, valeur + tempsExecutionComposant);
                }
            }

            // Ajouter le répertoire contenant du S3Composant si le dictionnaire ne le
            // contient pas déjà
            if (!mapRepertoiresS3.containsKey(repertoire)) {
                mapRepertoiresS3.put(repertoire, tempsExecutionComposant);
            }

            // Mettre à jour le nombre de répertoires
            nbRepertoires = Long.valueOf(mapRepertoiresS3.size());
            // Mettre à jour le temps d'exécution total des répertoires
            Long sommeValeurs = Long.valueOf(0);
            for (Long valeur : mapRepertoiresS3.values()) {
                sommeValeurs += valeur;
            }
            tempsExecutionTotalRepertoires = sommeValeurs;
        }
    }

    /**
     * Obtenir le temps d'exécution total de tous les répertoires.
     * 
     * @return Long : temps d'exécution total de tous les répertoires.
     */
    public Long getTempsExecutionTotalRepertoires() {
        return this.tempsExecutionTotalRepertoires;
    }

    /**
     * Obtenir le nombre total de répertoires scannés.
     * 
     * @return Long : nombre total de répertoires scannés.
     */
    public Long getNbRepertoires() {
        return this.nbRepertoires;
    }

    /**
     * Obtenir le temps d'exécution total de tous les fichiers.
     * 
     * @return Long : temps d'exécution total de tous les fichiers.
     */
    public Long getTempsExecutionTotalFichiers() {
        return this.tempsExecutionTotalFichiers;
    }

    /**
     * Obtenir le nombre total de fichiers scannés.
     * 
     * @return Long : nombre total de fichiers scannés.
     */
    public Long getNbFichiers() {
        return this.nbFichiers;
    }

    @Override
    public boolean getCanAdd() {
        return true;
    }

}
