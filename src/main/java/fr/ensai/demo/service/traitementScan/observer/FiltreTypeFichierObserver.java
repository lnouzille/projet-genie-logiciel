package fr.ensai.demo.service.traitementScan.observer;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.service.traitementScan.composite.Composant;
import fr.ensai.demo.service.traitementScan.composite.FichierComposant;
import fr.ensai.demo.service.traitementScan.composite.S3Composant;

public class FiltreTypeFichierObserver implements Observer {

    private String filtreTypeFichier;
    private boolean canAdd;

    /**
     * Constructeur.
     * 
     * @param filtreTypeFichier : filtre sur le type de fichier.
     */
    public FiltreTypeFichierObserver(String filtreTypeFichier) {
        this.filtreTypeFichier = filtreTypeFichier;
        this.canAdd = true;
    }

    /**
     * Mettre à jour la variable canAdd de l'observateur.
     * - Si le composant contient le filtre sur le type de fichier, elle est mise à
     * jour à True.
     * - Sinon, elle est mise à jour à False.
     * 
     * @param c : composant dont l'état a changé.
     */
    @Override
    public void update(Composant c) {
        if (c instanceof FichierComposant) {
            Fichier fichier = ((FichierComposant) c).getInfos();
            String typeFichier = fichier.getTypeFichier();
            if ((typeFichier == null) || (!typeFichier.contains(filtreTypeFichier))) {
                canAdd = false;
            } else {
                canAdd = true;
            }
        } else if (c instanceof S3Composant) {
            Fichier fichier = ((S3Composant) c).getInfos();
            String typeFichier = fichier.getTypeFichier();
            if ((typeFichier == null) || (!typeFichier.contains(filtreTypeFichier))) {
                canAdd = false;
            } else {
                canAdd = true;
            }
        }
    }

    /**
     * Récupérer le booléen qui sert à définir si le fichier peut être ajouté à la
     * liste des fichiers scannés en fonction du critère traité par l'observateur.
     * 
     * @return boolean : true si le fichier peut être ajouté et false sinon.
     */
    @Override
    public boolean getCanAdd() {
        return this.canAdd;
    }
}
