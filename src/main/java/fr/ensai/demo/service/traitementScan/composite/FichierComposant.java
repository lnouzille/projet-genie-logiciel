package fr.ensai.demo.service.traitementScan.composite;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.service.traitementScan.observer.Observer;
import fr.ensai.demo.service.traitementScan.observer.TempsExecutionObserver;

public class FichierComposant implements Composant {

    private File file;
    private int profondeurArbo;
    private boolean canAdd;
    private Long tempsExecution;
    private List<Fichier> fichiersCommuns;
    private Fichier infosFichier;
    private List<Observer> observers = new ArrayList<Observer>();

    /**
     * Constructeur.
     * 
     * @param file            : fichier sous la forme d'un objet File.
     * @param fichiersCommuns : liste des fichiers collectés lors du traitement du
     *                        scan.
     */
    public FichierComposant(File file, List<Fichier> fichiersCommuns) {
        this.file = file;
        this.fichiersCommuns = fichiersCommuns;
        this.canAdd = true;
    }

    /**
     * Collecter les informations sur le Composant et en notifier les observateurs.
     */
    @Override
    public void traiter() {

        long debutTraitement = System.currentTimeMillis();

        // Récolter les informations sur les fichiers et les stocker dans un objet
        // Fichier
        infosFichier = new Fichier();
        infosFichier.setNomFichier(file.getName()); // Nom du fichier
        infosFichier.setDateModification(new Date(file.lastModified())); // Date de modification
        infosFichier.setPoids(file.length()); // Poids
        infosFichier
                .setRepertoireContenant(file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf('/'))); // Répertoire
                                                                                                                       // contenant

        try {
            String mimeType = Files.probeContentType(file.toPath()); // Type de fichier
            infosFichier.setTypeFichier(mimeType);
        } catch (IOException e) {
            System.out
                    .println("Erreur lors de la récupération du type MIME du fichier : " + e.getMessage());
        }

        long finTraitement = System.currentTimeMillis();

        // Calculer le temps d'exécution
        tempsExecution = finTraitement - debutTraitement;

        // Informer les observateurs
        notifyObservers(this);

        // Ajouter le fichier à la liste des fichiers scannés s'il répond aux critères
        addInfosFichier(infosFichier);
    }

    /**
     * Ajouter un fichier à la liste des fichiers scannés s'il satisfait tous les
     * critères.
     * 
     * @param fichier : fichier à ajouter.
     */
    @Override
    public void addInfosFichier(Fichier fichier) {

        // Vérifier si les observateurs autorisent à ajouter le fichier
        for (Observer o : observers) {
            if (!(o instanceof TempsExecutionObserver)) {
                boolean reponse = o.getCanAdd();
                if (this.canAdd && !reponse) {
                    canAdd = false;
                    break; // Dès qu'un observateur n'autorise pas l'ajout du fichier, on arrête la boucle
                }
            }
        }

        // Ajouter le fichier si c'est le cas
        if (canAdd) {
            fichiersCommuns.add(fichier);
        }
    }

    /**
     * Récupérer les informations d'un fichier via l'objet Fichier.
     * 
     * @return Fichier : informations du fichier.
     */
    public Fichier getInfos() {
        return this.infosFichier;
    }

    /**
     * Ajouter un observateur.
     * 
     * @param o : observateur à ajouter.
     */
    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    /**
     * Supprimer un observateur.
     * 
     * @param o : observateur à supprimer.
     */
    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    /**
     * Informer les observateurs en appelant leur méthode de mise à jour avec le
     * composant spécifié.
     * 
     * @param c : composant dont l'état a changé.
     */
    @Override
    public void notifyObservers(Composant c) {
        for (Observer o : observers) {
            o.update(c);
        }
    }

    /**
     * Obtenir la profondeur du Composant dans l'arborescence.
     * 
     * @return int : profondeur du Composant dans l'arborescence.
     */
    @Override
    public int getProfondeurArbo() {
        return this.profondeurArbo;
    }

    /**
     * Définir la profondeur du Composant dans l'arborescence.
     * 
     * @param profondeurArbo : profondeur du Composant dans l'arborescence.
     */
    @Override
    public void setProfondeurArbo(int profondeurArbo) {
        this.profondeurArbo = profondeurArbo;
    }

    /**
     * Récupérer la liste des fichiers scannés.
     * 
     * @return List<Fichier> : liste des fichiers scannés.
     */
    @Override
    public List<Fichier> getInfosFichier() {
        return this.fichiersCommuns;
    }

    /**
     * Récupérer le booléen qui sert à définir si le fichier peut être ajouté à la
     * liste des fichiers scannés (s'il correspond bien aux critères).
     * 
     * @return boolean : true si le fichier peut être ajouté et false sinon.
     */
    @Override
    public boolean getCanAdd() {
        return this.canAdd;
    }

    /**
     * Obtenir le temps d'exécution du Composant.
     * 
     * @return Long : temps d'exécution du Composant.
     */
    @Override
    public Long getTempsExecution() {
        return this.tempsExecution;
    }

}
