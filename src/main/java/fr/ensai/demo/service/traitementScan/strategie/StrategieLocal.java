package fr.ensai.demo.service.traitementScan.strategie;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.service.traitementScan.composite.Composant;
import fr.ensai.demo.service.traitementScan.composite.DossierComposite;

public class StrategieLocal implements Strategie {

    /**
     * Récupérer le Composant initialisé.
     * 
     * @param scan : informations sur le scan.
     * @return Composant :
     *         composant initialisé et adapté au système de fichier local.
     */
    @Override
    public Composant getComposant(Scan scan) {

        // Initialisation du dossier racine
        String directoryPath = scan.getRepertoireInitial();
        File directory = createFile(directoryPath);

        // Initialisation de la liste des fichiers récupérés lors du scan
        List<Fichier> fichiersCommuns = new ArrayList<>();

        if (directory.exists() && directory.isDirectory()) { // La racine donnée est bien un dossier

            // Créer et retourner le dossier racine à explorer
            Composant racine = new DossierComposite(directory, fichiersCommuns);
            return racine;

        } else { // Sinon, retourner null
            return null;
        }
    }

    /**
     * Créer un fichier à partir d'un chemin.
     * Existe uniquement pour faciliter les tests.
     * 
     * @param path : chemin du fichier.
     * @return File : fichier créé.
     */
    protected File createFile(String filename) {
        return new File(filename);
    }
}
