package fr.ensai.demo.service.traitementScan.observer;

import java.util.List;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.service.traitementScan.composite.Composant;

public class MaxFichiersObserver implements Observer {

    private int nbMaxFichiers;
    private boolean canAdd;

    /**
     * Constructeur.
     * 
     * @param nbMaxFichiers : nombre maximum de fichiers à scanner.
     */
    public MaxFichiersObserver(int nbMaxFichiers) {
        this.nbMaxFichiers = nbMaxFichiers;
    }

    /**
     * Mettre à jour la variable canAdd de l'observateur.
     * - Si le nombre maximum de fichiers à scanner n'est pas encore atteint, elle
     * est mise à jour à True.
     * - Sinon, elle est mise à jour à False.
     * 
     * @param c : composant dont l'état a changé.
     */
    @Override
    public void update(Composant c) {
        List<Fichier> fichiersCommuns = c.getInfosFichier();
        int listSize = fichiersCommuns.size();
        if (listSize < this.nbMaxFichiers) {
            canAdd = true;
        } else {
            canAdd = false;
        }
    }

    /**
     * Récupérer le booléen qui sert à définir si le fichier peut être ajouté à la
     * liste des fichiers scannés en fonction du critère traité par l'observateur.
     * 
     * @return boolean : true si le fichier peut être ajouté et false sinon.
     */
    @Override
    public boolean getCanAdd() {
        return this.canAdd;
    }
}
