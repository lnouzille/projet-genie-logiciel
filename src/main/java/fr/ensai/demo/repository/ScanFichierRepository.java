package fr.ensai.demo.repository;

import fr.ensai.demo.model.ScanFichier;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScanFichierRepository extends CrudRepository<ScanFichier, Long> {

    /**
     * Rechercher la liste des couples (scan, fichier) associés à un scan
     * spécifique.
     * 
     * @param idScan : identifiant du scan.
     * @return List<ScanFichier> :
     *         liste des couples (scan, fichier) associés à un scan.
     */
    public List<ScanFichier> findByScanIdScan(Long idScan);

    /**
     * Supprimer la liste des couples (scan, fichier) associés à un scan spécifique.
     * 
     * @param idScan : identifiant du scan.
     */
    public void deleteByScanIdScan(Long idScan);
}
