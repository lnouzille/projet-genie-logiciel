package fr.ensai.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.ensai.demo.model.StatistiquesScan;

@Repository
public interface StatistiquesScanRepository extends CrudRepository<StatistiquesScan, Long> {

    /**
     * Rechercher les statistiques associées à un scan spécifique.
     * 
     * @param idScan : identifiant du scan.
     * @return List<StatistiquesScan> : statistiques associées à un scan.
     */
    public List<StatistiquesScan> findByScanIdScan(Long idScan);

    /**
     * Supprimer les statistiques associées à un scan spécifique.
     * 
     * @param idScan : identifiant du scan.
     */
    public void deleteByScanIdScan(Long idScan);

}
