package fr.ensai.demo.repository;

import fr.ensai.demo.model.Fichier;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FichierRepository extends CrudRepository<Fichier, Long> {

    /**
     * Rechercher un fichier par son nom, son répertoire contenant et sa date de
     * modification.
     * 
     * @param nomFichier          : nom du fichier.
     * @param repertoireContenant : répertoire contenant.
     * @param dateModification    : dernière date de modification.
     * @return Fichier : Fichier correspondant aux critères de recherche.
     */
    public Fichier findByNomFichierAndRepertoireContenantAndDateModification(
            String nomFichier, String repertoireContenant, Date dateModification);
}
