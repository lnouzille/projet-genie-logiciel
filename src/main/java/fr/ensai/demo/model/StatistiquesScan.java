package fr.ensai.demo.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "statistiques_scan")
public class StatistiquesScan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_scan", nullable = false)
    private Scan scan;

    @Column(name = "temps_execution_total_repertoires", nullable = false)
    private Long tempsExecutionTotalRepertoires;

    @Column(name = "nb_repertoires_scannes", nullable = false)
    private Long nbRepertoiresScannes;

    @Column(name = "temps_execution_total_fichiers", nullable = false)
    private Long tempsExecutionTotalFichiers;

    @Column(name = "nb_fichiers_scannes", nullable = false)
    private Long nbFichiersScannes;

    /**
     * Constructeur par défaut.
     */
    public StatistiquesScan() {

    }

    /**
     * Constructeur avec tous les paramètres.
     * 
     * @param id                             : identifiant de la table
     *                                       StatistiquesScan.
     * @param scan                           : scan.
     * @param tempsExecutionTotalRepertoires : temps d'exécution total des
     *                                       répertoires lors du scan.
     * @param nbRepertoiresScannes           : nombre total de répertoires scannés.
     * @param tempsExecutionTotalFichiers    : temps d'exécution total des fichiers
     *                                       lors du scan.
     * @param nbFichiersScannes              : nombre total de fichiers scannés.
     */
    public StatistiquesScan(Long id, Scan scan, Long tempsExecutionTotalRepertoires, Long nbRepertoiresScannes,
            Long tempsExecutionTotalFichiers, Long nbFichiersScannes) {
        this.id = id;
        this.scan = scan;
        this.tempsExecutionTotalRepertoires = tempsExecutionTotalRepertoires;
        this.nbRepertoiresScannes = nbRepertoiresScannes;
        this.tempsExecutionTotalFichiers = tempsExecutionTotalFichiers;
        this.nbFichiersScannes = nbFichiersScannes;
    }

    /**
     * Constructeur sans l'identifiant.
     * 
     * @param scan                           : scan.
     * @param tempsExecutionTotalRepertoires : temps d'exécution total des
     *                                       répertoires lors du scan.
     * @param nbRepertoiresScannes           : nombre total de répertoires scannés.
     * @param tempsExecutionTotalFichiers    : temps d'exécution total des fichiers
     *                                       lors du scan.
     * @param nbFichiersScannes              : nombre total de fichiers scannés.
     */
    public StatistiquesScan(Scan scan, Long tempsExecutionTotalRepertoires, Long nbRepertoiresScannes,
            Long tempsExecutionTotalFichiers, Long nbFichiersScannes) {
        this(null, scan, tempsExecutionTotalRepertoires, nbRepertoiresScannes, tempsExecutionTotalFichiers,
                nbFichiersScannes);
    }

    // Getters and setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Scan getScan() {
        return scan;
    }

    public void setScan(Scan scan) {
        this.scan = scan;
    }

    public Long getTempsExecutionTotalRepertoires() {
        return tempsExecutionTotalRepertoires;
    }

    public void setTempsExecutionTotalRepertoires(Long tempsExecutionTotalRepertoires) {
        this.tempsExecutionTotalRepertoires = tempsExecutionTotalRepertoires;
    }

    public Long getNbRepertoiresScannes() {
        return nbRepertoiresScannes;
    }

    public void setNbRepertoiresScannes(Long nbRepertoiresScannes) {
        this.nbRepertoiresScannes = nbRepertoiresScannes;
    }

    public Long getTempsExecutionTotalFichiers() {
        return tempsExecutionTotalFichiers;
    }

    public void setTempsExecutionTotalFichiers(Long tempsExecutionTotalFichiers) {
        this.tempsExecutionTotalFichiers = tempsExecutionTotalFichiers;
    }

    public Long getNbFichiersScannes() {
        return nbFichiersScannes;
    }

    public void setNbFichiersScannes(Long nbFichiersScannes) {
        this.nbFichiersScannes = nbFichiersScannes;
    }
}
