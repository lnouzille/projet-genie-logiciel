package fr.ensai.demo.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import java.util.Date;

@Entity
@Table(name = "fichier")
public class Fichier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long idFichier;

    @NotEmpty(message = "Nom de fichier manquant")
    @Column(name = "nom_fichier")
    private String nomFichier;

    @Column(name = "date_modification")
    private Date dateModification;

    @Column(name = "poids")
    private Long poids;

    @Column(name = "type_fichier")
    private String typeFichier;

    @Column(name = "repertoire_contenant")
    private String repertoireContenant;

    /**
     * Constructeur par défaut.
     */
    public Fichier() {
    }

    /**
     * Constructeur avec tous les paramètres.
     * 
     * @param idFichier           : identifiant du fichier.
     * @param nomFichier          : nom du fichier.
     * @param dateModification    : dernière date de modification.
     * @param poids               : poids en bytes.
     * @param typeFichier         : type de fichier.
     * @param repertoireContenant : répertoire contenant.
     */
    public Fichier(Long idFichier, String nomFichier, Date dateModification, Long poids, String typeFichier,
            String repertoireContenant) {
        this.idFichier = idFichier;
        this.nomFichier = nomFichier;
        this.dateModification = dateModification;
        this.poids = poids;
        this.typeFichier = typeFichier;
        this.repertoireContenant = repertoireContenant;
    }

    /**
     * Constructeur sans l'identifiant.
     * 
     * @param nomFichier          : nom du fichier.
     * @param dateModification    : dernière date de modification.
     * @param poids               : poids en bytes.
     * @param typeFichier         : type de fichier.
     * @param repertoireContenant : répertoire contenant.
     */
    public Fichier(String nomFichier, Date dateModification, Long poids, String typeFichier,
            String repertoireContenant) {
        this(null, nomFichier, dateModification, poids, typeFichier, repertoireContenant);
    }

    // Getters et setters

    public Long getIdFichier() {
        return this.idFichier;
    }

    public void setIdFichier(Long idFichier) {
        this.idFichier = idFichier;
    }

    public String getNomFichier() {
        return this.nomFichier;
    }

    public void setNomFichier(String nomFichier) {
        this.nomFichier = nomFichier;
    }

    public Date getDateModification() {
        return this.dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public Long getPoids() {
        return this.poids;
    }

    public void setPoids(Long poids) {
        this.poids = poids;
    }

    public String getTypeFichier() {
        return this.typeFichier;
    }

    public void setTypeFichier(String typeFichier) {
        this.typeFichier = typeFichier;
    }

    public String getRepertoireContenant() {
        return this.repertoireContenant;
    }

    public void setRepertoireContenant(String repertoireContenant) {
        this.repertoireContenant = repertoireContenant;
    }
}
