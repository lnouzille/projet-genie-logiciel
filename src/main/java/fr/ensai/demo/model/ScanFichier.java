package fr.ensai.demo.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.JoinColumn;

@Entity
@Table(name = "scan_fichier")
public class ScanFichier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_scan", nullable = false)
    private Scan scan;

    @ManyToOne
    @JoinColumn(name = "id_fichier", nullable = false)
    private Fichier fichier;

    /**
     * Constructeur par défaut.
     */
    public ScanFichier() {
    }

    /**
     * Constructeur sans l'identifiant.
     * 
     * @param scan    : scan.
     * @param fichier : fichier associé au scan.
     */
    public ScanFichier(Scan scan, Fichier fichier) {
        this.scan = scan;
        this.fichier = fichier;
    }

    // Getters et setters

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Scan getScan() {
        return this.scan;
    }

    public void setScan(Scan scan) {
        this.scan = scan;
    }

    public Fichier getFichier() {
        return this.fichier;
    }

    public void setFichier(Fichier fichier) {
        this.fichier = fichier;
    }

}
