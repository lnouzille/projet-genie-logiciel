package fr.ensai.demo.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "scan")
public class Scan {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long idScan;

    @NotEmpty(message = "Répertoire initial manquant")
    @Column(name = "repertoire_initial")
    private String repertoireInitial;

    @NotEmpty(message = "Statégie local ou S3 manquante")
    @Column(name = "scan_local")
    private boolean scanLocal;

    @Column(name = "nb_max_fichiers")
    private int nbMaxFichiers;

    @Column(name = "profondeur_max_arbo")
    private int profondeurMaxArbo;

    @Column(name = "filtre_nom_fichier")
    private String filtreNomFichier;

    @Column(name = "filtre_type_fichier")
    private String filtreTypeFichier;

    @JsonIgnore
    @Column(name = "date_scan")
    private Date dateScan;

    @JsonIgnore
    @Column(name = "temps_execution")
    private Long tempsExecution;

    /**
     * Constructeur par défaut.
     */
    public Scan() {
    }

    /**
     * Constructeur avec tous les paramètres.
     * 
     * @param idScan            : identifiant du scan.
     * @param repertoireInitial : répertoire initial à scanner ou nom du bucket S3.
     * @param scanLocal         : booléen qui spécifie si le scan s'effectue sur le
     *                          système de fichiers local ou de AWS S3.
     * @param nbMaxFichiers     : nombre maximum de fichiers à scanner.
     * @param profondeurMaxArbo : profondeur maximale dans l'arborescence.
     * @param filtreNomFichier  : filtre sur le nom de fichier.
     * @param filtreTypeFichier : filtre sur le type de fichier.
     * @param dateScan          : date du scan.
     * @param tempsExecution    : temps d'exécution du scan.
     */
    public Scan(Long idScan, String repertoireInitial, boolean scanLocal, int nbMaxFichiers, int profondeurMaxArbo,
            String filtreNomFichier,
            String filtreTypeFichier, Date dateScan, Long tempsExecution) {
        this.idScan = idScan;
        this.repertoireInitial = repertoireInitial;
        this.scanLocal = scanLocal;
        this.nbMaxFichiers = nbMaxFichiers;
        this.profondeurMaxArbo = profondeurMaxArbo;
        this.filtreNomFichier = filtreNomFichier;
        this.filtreTypeFichier = filtreTypeFichier;
        this.dateScan = dateScan;
        this.tempsExecution = tempsExecution;
    }

    /**
     * Constructeur sans l'identifiant.
     * 
     * @param repertoireInitial : répertoire initial à scanner ou nom du bucket S3.
     * @param scanLocal         : booléen qui spécifie si le scan s'effectue sur le
     *                          système de fichiers local ou de AWS S3.
     * @param nbMaxFichiers     : nombre maximum de fichiers à scanner.
     * @param profondeurMaxArbo : profondeur maximale dans l'arborescence.
     * @param filtreNomFichier  : filtre sur le nom de fichier.
     * @param filtreTypeFichier : filtre sur le type de fichier.
     * @param dateScan          : date du scan.
     * @param tempsExecution    : temps d'exécution du scan.
     */
    public Scan(String repertoireInitial, boolean scanLocal, int nbMaxFichiers, int profondeurMaxArbo,
            String filtreNomFichier,
            String filtreTypeFichier,
            Date dateScan, Long tempsExecution) {
        this(null, repertoireInitial, scanLocal, nbMaxFichiers, profondeurMaxArbo, filtreNomFichier, filtreTypeFichier,
                dateScan,
                tempsExecution);
    }

    /**
     * Dupliquer un scan.
     * 
     * @return Scan : scan dupliqué.
     */
    public Scan duplicate() {
        return new Scan(this.repertoireInitial, this.scanLocal, this.nbMaxFichiers, this.profondeurMaxArbo,
                this.filtreNomFichier,
                this.filtreTypeFichier,
                this.dateScan, this.tempsExecution);
    }

    // Getters et setters

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public Long getIdScan() {
        return this.idScan;
    }

    public void setIdScan(Long idScan) {
        this.idScan = idScan;
    }

    public String getRepertoireInitial() {
        return this.repertoireInitial;
    }

    public void setRepertoireInitial(String repertoireInitial) {
        this.repertoireInitial = repertoireInitial;
    }

    public boolean isScanLocal() {
        return this.scanLocal;
    }

    public void setScanLocal(boolean scanLocal) {
        this.scanLocal = scanLocal;
    }

    public int getNbMaxFichiers() {
        return this.nbMaxFichiers;
    }

    public void setNbMaxFichiers(int nbMaxFichiers) {
        this.nbMaxFichiers = nbMaxFichiers;
    }

    public int getProfondeurMaxArbo() {
        return this.profondeurMaxArbo;
    }

    public void setProfondeurMaxArbo(int profondeurMaxArbo) {
        this.profondeurMaxArbo = profondeurMaxArbo;
    }

    public String getFiltreNomFichier() {
        return this.filtreNomFichier;
    }

    public void setFiltreNomFichier(String filtreNomFichier) {
        this.filtreNomFichier = filtreNomFichier;
    }

    public String getFiltreTypeFichier() {
        return this.filtreTypeFichier;
    }

    public void setFiltreTypeFichier(String filtreTypeFichier) {
        this.filtreTypeFichier = filtreTypeFichier;
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public Date getDateScan() {
        return this.dateScan;
    }

    public void setDateScan(Date dateScan) {
        this.dateScan = dateScan;
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public Long getTempsExecution() {
        return this.tempsExecution;
    }

    public void setTempsExecution(Long tempsExecution) {
        this.tempsExecution = tempsExecution;
    }
}
