package fr.ensai.demo.dto;

public class DupliquerScanChampDto {

    private String champ;
    private String valeur;

    /**
     * Renvoyer le nom du champ à modifier.
     * 
     * @return String : nom du champ.
     */
    public String getChamp() {
        return champ;
    }

    /**
     * Renvoyer la valeur du champ à modifier.
     * 
     * @return String : valeur du champ.
     */
    public String getValeur() {
        return valeur;
    }

}
