package fr.ensai.demo.dto;

import java.util.List;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.model.StatistiquesScan;

public class ScanResultat {

    private Scan scan;
    private List<Fichier> fichiers;
    private StatistiquesScan statistiques;

    /**
     * Constructeur.
     * 
     * @param scan         : scan.
     * @param fichiers     : liste des fichiers scannés.
     * @param statistiques : statistiques du scan.
     */
    public ScanResultat(Scan scan, List<Fichier> fichiers, StatistiquesScan statistiques) {
        this.scan = scan;
        this.fichiers = fichiers;
        this.statistiques = statistiques;
    }

    // Getters et setters

    public Scan getScan() {
        return this.scan;
    }

    public void setScan(Scan scan) {
        this.scan = scan;
    }

    public List<Fichier> getfichiers() {
        return this.fichiers;
    }

    public void setfichiers(List<Fichier> fichiers) {
        this.fichiers = fichiers;
    }

    public StatistiquesScan getStatistiques() {
        return this.statistiques;
    }

    public void setStatistiques(StatistiquesScan statistiques) {
        this.statistiques = statistiques;
    }

}
