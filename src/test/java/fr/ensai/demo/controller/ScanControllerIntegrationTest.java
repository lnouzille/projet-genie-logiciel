package fr.ensai.demo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Date;
import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ensai.demo.DemoApplication;
import fr.ensai.demo.model.Scan;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = DemoApplication.class)
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class ScanControllerIntegrationTest {
        @Autowired
        private MockMvc mvc;

        @Autowired
        private ObjectMapper objectMapper;

        @BeforeEach
        public void setUp() throws Exception {
                Date date = Date.valueOf(LocalDate.now());
                Scan validScan1 = new Scan(1L, "src/", true, 20, 2, "aucun", "aucun", date, 50L);
                Scan validScan2 = new Scan(2L, "src/main/", true, 20, 4, "aucun", "aucun", date, 100L);

                mvc.perform(post("/scans")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(validScan1)))
                                .andExpect(status().isCreated());

                mvc.perform(post("/scans")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(validScan2)))
                                .andExpect(status().isCreated());
        }

        // Créer un scan
        // ===============================================================================================
        @Test
        public void newScan_WithValidScan_ShouldReturnCreated() throws Exception {
                Date date = Date.valueOf(LocalDate.now());
                Scan validScan3 = new Scan(3L, "src/main/java", true, 50, 2, "aucun", "aucun", date, 100L); 
                Scan validScan4 = new Scan(4L, "src/main/java/fr", true, 50, 2, "aucun", "aucun", date, 100L); 
                mvc.perform(post("/scans")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(validScan3)))
                                .andExpect(status().isCreated());

                mvc.perform(post("/scans")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(validScan4)))
                                .andExpect(status().isCreated());
        }

        @Test
        public void newScan_WithInvalidScan_ShouldReturnBadRequest() throws Exception {
                Date date = Date.valueOf(LocalDate.now());
                Scan invalidScan = new Scan(1L, "/src/", true, 50, 2, "aucun", "aucun", date, null);
                mvc.perform(post("/scans")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(invalidScan)))
                                .andExpect(status().isBadRequest())
                                .andExpect(content().string("Le répertoire initial donné est invalide."));
        }

        // Obtenir des scans
        // ===============================================================================================
        @Test
        public void getScans_ShouldReturnScans() throws Exception {
                mvc.perform(get("/scans")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk());
        }

        // Rejouer des scans
        // ===============================================================================================
        // @Test
        // public void rejouerScan_WithExistingId_ShouldReturnScan() throws Exception {
        // Long existingId = 1L;
        // mvc.perform(patch("/rejouer/" + existingId)
        // .contentType(MediaType.APPLICATION_JSON))
        // .andExpect(status().isOk());
        // }

        @Test
        public void rejouerScan_WithNonExistingId_ShouldReturnNotFound() throws Exception {
                Long nonExistingId = 999L; // Assume this ID does not exist
                mvc.perform(patch("/rejouer/" + nonExistingId)
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound());
        }

        // Obtenir des scans spécifiques
        // ===============================================================================================
        @Test
        public void getScan_ShouldReturnScan() throws Exception {
                mvc.perform(get("/scans/1")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk());
        }

        @Test
        public void getScan_ShouldReturnNotFound() throws Exception {
                mvc.perform(get("/scans/2000")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound());
        }

        @Test
        public void getScan_ShouldReturnBadRequest() throws Exception {
                mvc.perform(get("/scans/abc")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isBadRequest());
        }

        // Requeter les fichier
        // ===============================================================================================
        @Test
        public void getFichiers_ShouldReturnFichiers() throws Exception {
                mvc.perform(get("/fichiers")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                                .andExpect(jsonPath("$").isArray());
        }

        @Test
        public void getFichier_ShouldReturnNotFound() throws Exception {
                mvc.perform(get("/fichiers/5000")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound());
        }

        // Requerir des statistiques
        // ===============================================================================================
        @Test
        public void getStatistiquesScan_WithValidId_ShouldReturnStatistics() throws Exception {
                Long validId = 1L;
                mvc.perform(get("/scans/statistiques/" + validId)
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN));
        }

        @Test
        public void getStatistiquesScan_WithInvalidId_ShouldReturnNotFound() throws Exception {
                Long invalidId = 999L; // Assume this ID does not correspond to any existing scan
                mvc.perform(get("/scans/statistiques/" + invalidId)
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound());
        }

        @Test
        public void getStatistiques_ShouldReturnStatisticsForAllScans() throws Exception {
                mvc.perform(get("/scans/statistiques/all")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN));
        }

        // Requerir une comparaison
        // ===============================================================================================

        @Test
        public void comparaisonScans_WithInvalidIds_ShouldReturnNotFound() throws Exception {
                Long id1 = 999L;
                Long id2 = 998L;
                mvc.perform(get("/comparaison/" + id1 + "/" + id2)
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound());
        }

        // Requerir une suppression
        // ===============================================================================================

        @Test
        public void getInfosScan_WithInvalidId_ShouldReturnNotFound() throws Exception {
                Long invalidId = 999L;
                mvc.perform(get("/scans/infos/" + invalidId)
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound());
        }
}
