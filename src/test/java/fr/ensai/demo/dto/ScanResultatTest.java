package fr.ensai.demo.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.model.StatistiquesScan;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class ScanResultatTest {
    private ScanResultat scanResultat;
    private Scan scan1, scan2;
    private Fichier fichier1, fichier2, fichier3, fichier4;
    private List<Fichier> fichiers1, fichiers2;
    private StatistiquesScan statistiques1, statistiques2;

    @BeforeEach
    public void setUp() {
        Date now = new Date();
        scan1 = new Scan(1L, "src/", true, 10, 2, "aucun", "java", now, 20L);
        scan2 = new Scan(2L, "src/", true, 10, 2, "aucun", "java", now, 20L);

        fichier1 = new Fichier("Fichier1.java", now, 1024L, ".java", "model/");
        fichier2 = new Fichier("Fichier2.java", now, 1024L, ".java", "model/");
        fichier3 = new Fichier("Fichier3.java", now, 1024L, ".java", "model/");
        fichier4 = new Fichier("Fichier4.java", now, 1024L, ".java", "model/");

        fichiers1 = Arrays.asList(fichier1, fichier2);
        fichiers2 = Arrays.asList(fichier3, fichier4);

        statistiques1 = new StatistiquesScan();
        statistiques2 = new StatistiquesScan();

        scanResultat = new ScanResultat(scan1, fichiers1, statistiques1);
    }

    @Test
    public void testGettersAndSetters() {
        // Test pour 'getScan' et 'setScan'
        scanResultat.setScan(scan2);
        assertEquals(scan2, scanResultat.getScan());

        // Test pour 'getfichiers' et 'setfichiers'
        scanResultat.setfichiers(fichiers2);
        assertEquals(fichiers2, scanResultat.getfichiers());

        // Test pour 'getStatistiques' et 'setStatistiques'
        scanResultat.setStatistiques(statistiques2);
        assertEquals(statistiques2, scanResultat.getStatistiques());
    }
}
