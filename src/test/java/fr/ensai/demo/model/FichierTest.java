package fr.ensai.demo.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class FichierTest {

    @Test
    public void testConstructor1() {
        Date now = new Date();
        Fichier fichier1 = new Fichier("fichier1", now, 2048L, "txt", "/test/dir");

        assertEquals("fichier1", fichier1.getNomFichier());
        assertEquals(now, fichier1.getDateModification());
        assertEquals(2048L, fichier1.getPoids());
        assertEquals("txt", fichier1.getTypeFichier());
        assertEquals("/test/dir", fichier1.getRepertoireContenant());
    }

    @Test
    public void testConstructor2() {
        Date now = new Date();
        Fichier fichier2 = new Fichier(3L, "fichier2", now, 4096L, "docx", "test/dir");

        assertEquals(3L, fichier2.getIdFichier());
        assertEquals("fichier2", fichier2.getNomFichier());
        assertEquals(now, fichier2.getDateModification());
        assertEquals(4096L, fichier2.getPoids());
        assertEquals("docx", fichier2.getTypeFichier());
        assertEquals("test/dir", fichier2.getRepertoireContenant());
    }

    @Test
    public void testGettersAndSetters() {
        Fichier fichier = new Fichier();
        Date now = new Date();

        fichier.setIdFichier(1L);
        fichier.setNomFichier("test.txt");
        fichier.setDateModification(now);
        fichier.setPoids(1024L);
        fichier.setTypeFichier("text/plain");
        fichier.setRepertoireContenant("/test/directory");

        assertEquals(1L, fichier.getIdFichier());
        assertEquals("test.txt", fichier.getNomFichier());
        assertEquals(now, fichier.getDateModification());
        assertEquals(1024L, fichier.getPoids());
        assertEquals("text/plain", fichier.getTypeFichier());
        assertEquals("/test/directory", fichier.getRepertoireContenant());
    }
}
