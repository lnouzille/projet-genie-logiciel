package fr.ensai.demo.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class ScanTest {
    private Scan scan;

    @BeforeEach
    public void setUp() {
        scan = new Scan();
        scan.setIdScan(1L);
        scan.setScanLocal(true);
        scan.setNbMaxFichiers(100);
        scan.setProfondeurMaxArbo(5);
        scan.setFiltreNomFichier("test.txt");
        scan.setFiltreTypeFichier(".txt");
        scan.setDateScan(new Date());
        scan.setTempsExecution(120L);
    }

    @Test
    public void testGettersAndSetters() {
        assertEquals(Long.valueOf(1), scan.getIdScan());
        assertEquals(true, scan.isScanLocal());
        assertEquals(100, scan.getNbMaxFichiers());
        assertEquals(5, scan.getProfondeurMaxArbo());
        assertEquals("test.txt", scan.getFiltreNomFichier());
        assertEquals(".txt", scan.getFiltreTypeFichier());
        assertNotNull(scan.getDateScan());
        assertEquals(Long.valueOf(120), scan.getTempsExecution());
    }

    @Test
    public void testDuplicate() {
        Scan duplicateScan = scan.duplicate();
        assertNotSame(scan, duplicateScan);
        assertEquals(scan.isScanLocal(), duplicateScan.isScanLocal());
        assertEquals(scan.getNbMaxFichiers(), duplicateScan.getNbMaxFichiers());
        assertEquals(scan.getProfondeurMaxArbo(), duplicateScan.getProfondeurMaxArbo());
        assertEquals(scan.getFiltreNomFichier(), duplicateScan.getFiltreNomFichier());
        assertEquals(scan.getFiltreTypeFichier(), duplicateScan.getFiltreTypeFichier());
        assertEquals(scan.getDateScan(), duplicateScan.getDateScan());
        assertEquals(scan.getTempsExecution(), duplicateScan.getTempsExecution());
    }
}
