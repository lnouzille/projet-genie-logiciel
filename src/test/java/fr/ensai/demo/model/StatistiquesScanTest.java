package fr.ensai.demo.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class StatistiquesScanTest {

    private StatistiquesScan statistiquesScan;
    private Scan scanMock;

    @BeforeEach
    public void setUp() {
        scanMock = mock(Scan.class);
        statistiquesScan = new StatistiquesScan();
        statistiquesScan.setId(1L);
        statistiquesScan.setScan(scanMock);
        statistiquesScan.setTempsExecutionTotalRepertoires(100L);
        statistiquesScan.setNbRepertoiresScannes(5L);
        statistiquesScan.setTempsExecutionTotalFichiers(200L);
        statistiquesScan.setNbFichiersScannes(50L);
    }

    @Test
    public void testGettersAndSetters() {
        assertEquals(1L, statistiquesScan.getId());
        assertEquals(scanMock, statistiquesScan.getScan());
        assertEquals(100L, statistiquesScan.getTempsExecutionTotalRepertoires());
        assertEquals(5L, statistiquesScan.getNbRepertoiresScannes());
        assertEquals(200L, statistiquesScan.getTempsExecutionTotalFichiers());
        assertEquals(50L, statistiquesScan.getNbFichiersScannes());
    }

    @Test
    public void testConstructor1() {
        Scan scanMock2 = mock(Scan.class);
        Scan scanMock3 = mock(Scan.class);
        StatistiquesScan statistiquesScan2 = new StatistiquesScan(1L, scanMock2, 60L, 10L, 500L, 50L);
        StatistiquesScan statistiquesScan3 = new StatistiquesScan(scanMock3, 60L, 10L, 500L, 50L);

        assertNull(statistiquesScan3.getId());
        assertEquals(1L, statistiquesScan2.getId());
        assertEquals(scanMock2, statistiquesScan2.getScan());
        assertEquals(60L, statistiquesScan2.getTempsExecutionTotalRepertoires());
        assertEquals(10L, statistiquesScan2.getNbRepertoiresScannes());
        assertEquals(500L, statistiquesScan2.getTempsExecutionTotalFichiers());
        assertEquals(50L, statistiquesScan2.getNbFichiersScannes());
    }
}
