package fr.ensai.demo.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class ScanFichierTest {

    private ScanFichier scanFichier;
    private Scan scanMock;
    private Fichier fichierMock;

    @BeforeEach
    public void setUp() {
        scanMock = mock(Scan.class);
        fichierMock = mock(Fichier.class);
        scanFichier = new ScanFichier(scanMock, fichierMock);
    }

    @Test
    public void testGettersAndSetters() {
        // Test des setters
        Scan newScanMock = mock(Scan.class);
        Fichier newFichierMock = mock(Fichier.class);
        scanFichier.setScan(newScanMock);
        scanFichier.setFichier(newFichierMock);
        scanFichier.setId(10L);

        // Test des getters
        assertEquals(newScanMock, scanFichier.getScan());
        assertEquals(newFichierMock, scanFichier.getFichier());
        assertEquals(10L, scanFichier.getId());
    }
}
