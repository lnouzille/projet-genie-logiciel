package fr.ensai.demo.service.traitementScan.composite;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import fr.ensai.demo.model.Fichier;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class DossierCompositeTest {

    private DossierComposite dossierComposite;
    private File mockFile;

    @BeforeEach
    public void setUp() {
        mockFile = mock(File.class);
        List<Fichier> fichiersCommuns = new ArrayList<>();
        dossierComposite = new DossierComposite(mockFile, fichiersCommuns);
    }

    @Test
    public void testTempsExecution() {
        // Mocking the listFiles method to return an empty array
        when(mockFile.listFiles()).thenReturn(new File[] {});

        long startTime = System.currentTimeMillis();
        dossierComposite.traiter();
        long endTime = System.currentTimeMillis();

        // Assert that tempsExecution is within reasonable bounds
        assertTrue(dossierComposite.getTempsExecution() <= (endTime - startTime));
    }

    @Test
    public void testExplorerSousRepertoires() {
        // Preparer le mock
        File mockDir = mock(File.class);
        File mockFileInsideDir = mock(File.class);
        when(mockFile.listFiles()).thenReturn(new File[] { mockDir, mockFileInsideDir });
        when(mockDir.isDirectory()).thenReturn(true);
        when(mockFileInsideDir.isDirectory()).thenReturn(false);
        when(mockDir.listFiles()).thenReturn(new File[] {});

        dossierComposite.explorerSousRepertoires();

        // Vérifier que les composants ont été ajouté
        assertEquals(2, dossierComposite.getChildren().size());
    }

    @Test
    public void testAddetRemove() {
        Composant mockComposant = mock(FichierComposant.class);
        Fichier mockFichier = mock(Fichier.class);
        dossierComposite.add(mockComposant);
        assertEquals(1, dossierComposite.getChildren().size());

        dossierComposite.remove(mockComposant);
        assertEquals(0, dossierComposite.getChildren().size());

        dossierComposite.addInfosFichier(mockFichier);
        assertEquals(1, dossierComposite.getInfosFichier().size());
    }

    @Test
    public void testAutresGetterEtSetter() {
        Composant mockComposant = mock(FichierComposant.class);

        dossierComposite.add(mockComposant);
        assertTrue(dossierComposite.getCanAdd());

        dossierComposite.setProfondeurArbo(2);
        assertEquals(2, dossierComposite.getProfondeurArbo());
    }
}
