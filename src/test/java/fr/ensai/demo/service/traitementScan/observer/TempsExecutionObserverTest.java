package fr.ensai.demo.service.traitementScan.observer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.service.traitementScan.composite.DossierComposite;
import fr.ensai.demo.service.traitementScan.composite.FichierComposant;
import fr.ensai.demo.service.traitementScan.composite.S3Composant;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class TempsExecutionObserverTest {

    private TempsExecutionObserver observer;
    private FichierComposant fichierComposant;
    private DossierComposite dossierComposite;
    private S3Composant s3Composant;

    @BeforeEach
    public void setUp() {
        observer = new TempsExecutionObserver();
        fichierComposant = mock(FichierComposant.class);
        dossierComposite = mock(DossierComposite.class);
        s3Composant = mock(S3Composant.class);
    }

    @Test
    public void updateIncreasesFileExecutionTimeWhenFichierComposant() {
        when(fichierComposant.getTempsExecution()).thenReturn(100L);
        observer.update(fichierComposant);
        assertEquals(100L, observer.getTempsExecutionTotalFichiers());
    }

    @Test
    public void updateIncreasesDirectoryExecutionTimeWhenDossierComposite() {
        when(dossierComposite.getTempsExecution()).thenReturn(200L);
        observer.update(dossierComposite);
        assertEquals(200L, observer.getTempsExecutionTotalRepertoires());
    }

    @Test
    public void updateHandlesS3ComposantCorrectly() {
        when(s3Composant.getTempsExecution()).thenReturn(300L);
        when(s3Composant.getInfos()).thenReturn(mock(Fichier.class));
        observer.update(s3Composant);
        // Assuming s3Composant updates both file and directory execution times
        assertEquals(300L, observer.getTempsExecutionTotalFichiers());
        // Additional assertions can be made based on how S3Composant is expected to
        // update counts and times
    }
}
