package fr.ensai.demo.service.traitementScan.observer;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.demo.service.traitementScan.composite.Composant;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class ProfondeurArboObserverTest {

    private ProfondeurArboObserver observer;
    private Composant composant;

    @BeforeEach
    public void setUp() {
        observer = new ProfondeurArboObserver(5); // Assuming max depth is 5
        composant = mock(Composant.class);
    }

    @Test
    public void whenComposantDepthIsLessThanMax_thenCanAddIsTrue() {
        when(composant.getProfondeurArbo()).thenReturn(3);
        observer.update(composant);
        assertTrue(observer.getCanAdd());
    }

    @Test
    public void whenComposantDepthIsGreaterThanMax_thenCanAddIsFalse() {
        when(composant.getProfondeurArbo()).thenReturn(6);
        observer.update(composant);
        assertFalse(observer.getCanAdd());
    }
}
