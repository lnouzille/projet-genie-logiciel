package fr.ensai.demo.service.traitementScan.strategie;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.demo.model.Scan;
import fr.ensai.demo.service.traitementScan.composite.Composant;
import fr.ensai.demo.service.traitementScan.composite.DossierComposite;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class StrategieLocalTest {

    private StrategieLocal strategieLocal;

    @Mock
    private Scan mockScan;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        strategieLocal = new StrategieLocal();

        // Assuming Scan has a method getRepertoireInitial that returns a String path
        when(mockScan.getRepertoireInitial()).thenReturn("/valid/path/");
    }

    @Test
    public void whenDirectoryExists_thenShouldReturnDossierComposite() {
        // Prepare the File mock to simulate an existing directory
        File mockFile = mock(File.class);
        when(mockFile.exists()).thenReturn(true);
        when(mockFile.isDirectory()).thenReturn(true);
        strategieLocal = new StrategieLocal() {
            @Override
            protected File createFile(String path) {
                return mockFile; // Use the mock File instead of a real file
            }
        };

        Composant result = strategieLocal.getComposant(mockScan);

        assertNotNull(result);
        assertTrue(result instanceof DossierComposite);
    }

    @Test
    public void whenDirectoryDoesNotExist_thenShouldReturnNull() {
        // Prepare the File mock to simulate a non-existing directory
        File mockFile = mock(File.class);
        when(mockFile.exists()).thenReturn(false);
        strategieLocal = new StrategieLocal() {
            @Override
            protected File createFile(String path) {
                return mockFile; // Use the mock File instead of a real file
            }
        };

        Composant result = strategieLocal.getComposant(mockScan);

        assertNull(result);
    }
}
