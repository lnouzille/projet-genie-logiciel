package fr.ensai.demo.service.traitementScan.composite;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.demo.model.Fichier;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.S3Object;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class S3ComposantTest {
    private S3Composant s3Composant;
    private S3Client mockS3Client;

    @BeforeEach
    public void setUp() {
        Fichier fichier1 = mock(Fichier.class);
        Fichier fichier2 = mock(Fichier.class);
        List<Fichier> fichiersCommuns = Arrays.asList(fichier1, fichier2);
        List<Fichier> fichiersCommuns2 = new ArrayList<>(fichiersCommuns);
        mockS3Client = mock(S3Client.class);
        s3Composant = new S3Composant("bucketName", fichiersCommuns2) {
            @Override
            protected S3Client createS3Client() {
                return mockS3Client;
            }
        };
    }

    @Test
    public void traiterShouldListObjectsInBucket() {
        ListObjectsV2Response mockResponse = ListObjectsV2Response.builder()
                .contents(S3Object.builder().key("testFile.txt").size(1024L).lastModified(Instant.now()).build())
                .build();
        when(mockS3Client.listObjectsV2(any(ListObjectsV2Request.class))).thenReturn(mockResponse);

        // Executer la méthode traiter()
        s3Composant.traiter();

        // Vérification que la méthode listObjectsV2 a été appelée et que la liste des
        // fichiers est non vide
        verify(mockS3Client).listObjectsV2(any(ListObjectsV2Request.class));
        assertTrue(s3Composant.getInfosFichier().size() > 0);
        assertTrue(s3Composant.getCanAdd());
        assertTrue(s3Composant.getInfosFichier().size() > 0);
        assertTrue(s3Composant.getInfosFichier() instanceof List<Fichier>);
    }

    @Test
    public void testAutresGetterEtSetter() {
        s3Composant.setProfondeurArbo(2);
        assertEquals(2, s3Composant.getProfondeurArbo());
    }
}
