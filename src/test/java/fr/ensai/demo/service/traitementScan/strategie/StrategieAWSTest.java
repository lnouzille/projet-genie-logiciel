package fr.ensai.demo.service.traitementScan.strategie;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.demo.model.Scan;
import fr.ensai.demo.service.traitementScan.composite.Composant;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class StrategieAWSTest {
    @Mock
    private S3Client mockS3Client;

    private StrategieAWS strategieAWS;
    private Scan mockScan;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        strategieAWS = new StrategieAWS() {
            @Override
            protected S3Client createS3Client() {
                return mockS3Client;
            }
        };
        mockScan = mock(Scan.class);
        when(mockScan.getRepertoireInitial()).thenReturn("test-bucket");
    }

    @Test
    public void whenBucketDoesNotExist_thenShouldReturnNull() {
        when(mockS3Client.headBucket(any(HeadBucketRequest.class))).thenThrow(NoSuchBucketException.class);

        Composant result = strategieAWS.getComposant(mockScan);

        assertNull(result);
    }
}
