package fr.ensai.demo.service.traitementScan.observer;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.service.traitementScan.composite.FichierComposant;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class FiltreTypeFichierObserverTest {

    private FiltreTypeFichierObserver observer;
    private FichierComposant fichierComposant;
    private Fichier fichier;

    @BeforeEach
    public void setUp() {
        observer = new FiltreTypeFichierObserver("text/plain"); // Example filter
        fichier = mock(Fichier.class);
        fichierComposant = mock(FichierComposant.class);
        when(fichierComposant.getInfos()).thenReturn(fichier);
    }

    @Test
    public void whenFileTypeMatchesFilter_thenCanAddIsTrue() {
        when(fichier.getTypeFichier()).thenReturn("text/plain");

        observer.update(fichierComposant);

        assertTrue(observer.getCanAdd());
    }

    @Test
    public void whenFileTypeDoesNotMatchFilter_thenCanAddIsFalse() {
        when(fichier.getTypeFichier()).thenReturn("image/jpeg");

        observer.update(fichierComposant);

        assertFalse(observer.getCanAdd());
    }

    @Test
    public void whenFileTypeIsNull_thenCanAddIsFalse() {
        when(fichier.getTypeFichier()).thenReturn(null);

        observer.update(fichierComposant);

        assertFalse(observer.getCanAdd());
    }
}
