package fr.ensai.demo.service.traitementScan;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.demo.DemoApplication;
import fr.ensai.demo.dto.ScanResultat;
import fr.ensai.demo.model.Scan;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = DemoApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class AppliquerScannerIT {
    
    @Autowired
    private AppliquerScanner appliquerScanner;

    @Test
    public void getResultatScan_ShouldReturnValidResult() {
        Scan mockScan = new Scan("src/test/resources", true, 10, 2, "aucun", "aucun", null, null);
        
        ScanResultat resultat = appliquerScanner.getResultatScan(mockScan);

        assertNotNull(resultat);
    }
}
