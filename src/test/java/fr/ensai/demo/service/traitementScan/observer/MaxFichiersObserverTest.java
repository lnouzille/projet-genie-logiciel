package fr.ensai.demo.service.traitementScan.observer;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.service.traitementScan.composite.Composant;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class MaxFichiersObserverTest {

    private MaxFichiersObserver observer;
    private Composant composant;

    @BeforeEach
    public void setUp() {
        observer = new MaxFichiersObserver(5);
        composant = mock(Composant.class);
    }

    @Test
    public void whenNumberOfFilesIsLessThanMax_thenCanAddIsTrue() {
        List<Fichier> mockedFichiers = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            mockedFichiers.add(mock(Fichier.class));
        }
        when(composant.getInfosFichier()).thenReturn(mockedFichiers);

        observer.update(composant);
        assertTrue(observer.getCanAdd());
    }

    @Test
    public void whenNumberOfFilesIsEqualToMax_thenCanAddIsFalse() {
        List<Fichier> mockedFichiers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            mockedFichiers.add(mock(Fichier.class));
        }
        when(composant.getInfosFichier()).thenReturn(mockedFichiers);

        observer.update(composant);
        assertFalse(observer.getCanAdd());
    }

    @Test
    public void whenNumberOfFilesIsGreaterThanMax_thenCanAddIsFalse() {
        List<Fichier> mockedFichiers = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            mockedFichiers.add(mock(Fichier.class));
        }
        when(composant.getInfosFichier()).thenReturn(mockedFichiers);

        observer.update(composant);
        assertFalse(observer.getCanAdd());
    }
}
