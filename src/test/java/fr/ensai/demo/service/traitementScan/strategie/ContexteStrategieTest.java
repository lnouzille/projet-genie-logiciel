package fr.ensai.demo.service.traitementScan.strategie;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.demo.model.Scan;
import fr.ensai.demo.service.traitementScan.composite.Composant;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class ContexteStrategieTest {

    @Mock
    private Strategie mockStrategie;

    private ContexteStrategie contexteStrategie;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        contexteStrategie = new ContexteStrategie();
        contexteStrategie.setStrategie(mockStrategie);
    }

    @Test
    public void testExecuterStrategie() {
        Scan mockScan = mock(Scan.class);
        Composant expectedComposant = mock(Composant.class);

        when(mockStrategie.getComposant(any(Scan.class))).thenReturn(expectedComposant);

        Composant result = contexteStrategie.executerStrategie(mockScan);

        assertNotNull(result);
        verify(mockStrategie).getComposant(mockScan);
    }
}
