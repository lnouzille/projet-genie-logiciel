package fr.ensai.demo.service.traitementScan.observer;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.service.traitementScan.composite.FichierComposant;
import fr.ensai.demo.service.traitementScan.composite.S3Composant;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class FiltreNomFichierObserverTest {

    private FiltreNomFichierObserver observer;
    private FichierComposant fichierComposant;
    private S3Composant s3Composant;
    private Fichier fichier;

    @BeforeEach
    public void setUp() {
        observer = new FiltreNomFichierObserver("test");
        fichier = mock(Fichier.class);
        fichierComposant = mock(FichierComposant.class);
        when(fichierComposant.getInfos()).thenReturn(fichier);
    }

    @Test
    public void whenFichierNameContainsFilter_thenCanAddIsTrue() {
        when(fichier.getNomFichier()).thenReturn("testFile.txt");

        observer.update(fichierComposant);

        assertTrue(observer.getCanAdd());
    }

    @Test
    public void whenS3ComposantNameContainsFilter_thenCanAddIsTrue() {
        when(fichier.getNomFichier()).thenReturn("s3testFile.txt");

        observer.update(s3Composant);

        assertTrue(observer.getCanAdd());
    }

    @Test
    public void whenFichierNameDoesNotContainFilter_thenCanAddIsFalse() {
        when(fichier.getNomFichier()).thenReturn("file.txt");

        observer.update(fichierComposant);

        assertFalse(observer.getCanAdd());
    }
}
