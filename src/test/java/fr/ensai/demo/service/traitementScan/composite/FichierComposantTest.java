package fr.ensai.demo.service.traitementScan.composite;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import fr.ensai.demo.model.Fichier;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class FichierComposantTest {

    private FichierComposant fichierComposant;
    private File file;
    private List<Fichier> fichiersCommuns;

    @BeforeEach
    public void setUp() {
        file = mock(File.class); // Mock the File to avoid actual file system access
        fichiersCommuns = new ArrayList<>(); // Initialize with or without mock Fichier objects
        fichierComposant = new FichierComposant(file, fichiersCommuns);
    }

    @Test
    public void testAddInfosFichier() {
        Fichier fichier = mock(Fichier.class); // Mock the Fichier
        fichierComposant.addInfosFichier(fichier);

        assertTrue(fichierComposant.getInfosFichier().contains(fichier));
    }

    @Test
    public void testGettersAndSetters() {
        // Test getters and setters
        int expectedProfondeur = 5;
        fichierComposant.setProfondeurArbo(expectedProfondeur);
        assertEquals(expectedProfondeur, fichierComposant.getProfondeurArbo());
    }

}
