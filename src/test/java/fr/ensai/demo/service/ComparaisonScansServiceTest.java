package fr.ensai.demo.service;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.model.ScanFichier;
import fr.ensai.demo.repository.ScanFichierRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class ComparaisonScansServiceTest {

    @Mock
    private ScanFichierRepository scanFichierRepository;

    @Mock
    private ComparaisonScansService service;

    @InjectMocks
    private ComparaisonScansService comparaisonScansService;

    private Scan scan1, scan2;
    private Fichier fichier1, fichier2, fichierCommun1, fichierCommun2;
    List<Fichier> fichiersListe1, fichiersListe2;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DATE, -2); // subtract 2 days
        Date twoDaysBefore = cal.getTime();

        scan1 = new Scan(1L, "src/", true, 10, 2, "aucun", "java", now, 20L);

        fichier1 = new Fichier("Scan.java", twoDaysBefore, 1024L, ".java", "model/");
        fichier2 = new Fichier("Fichier.java", twoDaysBefore, 1024L, ".java", "model/");

        fichierCommun1 = new Fichier("Fichiercommun1.java", now, 1024L, ".java", "model/");
        fichierCommun2 = new Fichier("Fichiercommun2.java", now, 1024L, ".java", "model/");

        fichiersListe1 = Arrays.asList(fichier1, fichierCommun1, fichierCommun2);
        fichiersListe2 = Arrays.asList(fichier2, fichierCommun1, fichierCommun2);
    }

    @Test
    public void testGetInfos() {
        // Configuration les comportements simulés ici
        when(scanFichierRepository.findByScanIdScan(anyLong()))
                .thenReturn(Arrays.asList(new ScanFichier(scan1, fichier1)));

        // Test de la méthode getInfos
        Pair<Scan, List<Fichier>> result = comparaisonScansService.getInfos(1L);
        List<Fichier> fichiersAttendus = result.getRight();
        assertEquals(scan1, result.getLeft());

        // Vérifiez que la liste contient un seul élément
        assertEquals(1, fichiersAttendus.size(), "La liste doit contenir un seul fichier.");

        // Vérifiez que le fichier attendu est présent dans la liste
        assertTrue(fichiersAttendus.contains(fichier1));
    }

    public void getInfosThrowsNotFoundExceptionWhenScanDoesNotExist() {
        // Simuler le comportement pour renvoyer un Iterable vide
        when(scanFichierRepository.findByScanIdScan(1L)).thenReturn(new ArrayList<>());

        // Vérifier que l'exception est bien lancée avec le bon statut
        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> comparaisonScansService.getInfos(1L));
        assertEquals(HttpStatus.NOT_FOUND.value(), exception.getStatusCode());
        assertEquals("L'un des scans que vous voulez comparer n'existe pas.", exception.getReason());
    }

    @Test
    public void testGetParametres() {
        // Testez la méthode getParametres ici
        // Utilisez des assertions pour vérifier que la chaîne retournée contient les
        // informations attendues du scan
        Date now = new Date();
        scan2 = new Scan(2L, "src/", true, 15, 2, "aucun", "java", now, 20L);

        String params = comparaisonScansService.getParametres(scan2);
        String infosScan = "Scan n°" + 2L + "\n";
        infosScan += "---- Répertoire de départ : " + "src/" + "\n";
        infosScan += "---- Système de fichier local : " + true + "\n";
        infosScan += "---- Nombre max de fichiers : " + 15 + "\n";
        infosScan += "---- Profondeur max : " + 2 + "\n";
        infosScan += "---- Filtre sur le nom de fichier : " + "aucun" + "\n";
        infosScan += "---- Filtre sur le type de fichier : " + "java" + "\n";
        infosScan += "---- Date du scan : " + now + "\n";
        infosScan += "---- Temps d'exécution : " + 20L + " ms \n";

        assertEquals(params, infosScan);
    }

    @Test
    public void testGetFichiersCommuns() {
        // Appeler la méthode à tester
        List<Fichier> result = comparaisonScansService.getFichiersCommuns(fichiersListe1, fichiersListe2);

        // Vérifier le résultat
        assertEquals(2, result.size(), "La liste des fichiers communs doit contenir un seul fichier.");
        assertTrue(result.contains(fichierCommun1), "La liste des fichiers communs doit contenir le fichier commun.");
        assertTrue(result.contains(fichierCommun2), "La liste des fichiers communs doit contenir le fichier commun.");
    }

    @Test
    public void testGetFichiersCommunsText() {

        // Appel de la méthode à tester
        String resultat = comparaisonScansService.getFichiersCommunsText(fichiersListe1, fichiersListe2);

        // Vérifiez que le résultat contient le texte attendu pour le fichier commun
        String texteAttendu1 = "---- " + fichierCommun1.getNomFichier() + " | " + fichierCommun1.getDateModification()
                + " | " + fichierCommun2.getRepertoireContenant() + "\n";
        String texteAttendu2 = "---- " + fichierCommun2.getNomFichier() + " | " + fichierCommun2.getDateModification()
                + " | " + fichierCommun2.getRepertoireContenant() + "\n";
        assertTrue(resultat.contains(texteAttendu1));
        assertTrue(resultat.contains(texteAttendu2));
    }

    @Test
    public void testGetFichiersRestants() {
        List<Fichier> fichiersCommuns = Arrays.asList(fichierCommun1, fichierCommun2);
        String resultat = comparaisonScansService.getFichiersRestants(scan1, fichiersListe1, fichiersCommuns);

        assertTrue(resultat.contains("Scan.java"));
        assertTrue(!resultat.contains("Fichiercommun1.java"));
        assertTrue(resultat instanceof String);
    }

    @Test
    public void testComparaison() {
        // Configuration les comportements simulés ici
        Date now = new Date();
        scan2 = new Scan(2L, "src/", true, 15, 2, "aucun", "java", now, 20L);
        List<ScanFichier> scanFichiers1 = new ArrayList<>();
        List<ScanFichier> scanFichiers2 = new ArrayList<>();
        for (Fichier f : fichiersListe1) {
            scanFichiers1.add(new ScanFichier(scan1, f));
        }
        for (Fichier f : fichiersListe2) {
            scanFichiers2.add(new ScanFichier(scan2, f));
        }

        scanFichiers1.add(new ScanFichier(scan1, fichier1));
        when(scanFichierRepository.findByScanIdScan(1L)).thenReturn(scanFichiers1);
        when(scanFichierRepository.findByScanIdScan(2L)).thenReturn(scanFichiers2);

        // Exécuter la méthode à tester
        String resultat = comparaisonScansService.comparaison(1L, 2L);

        // Vérifier que le résultat contient les informations attendues
        assertTrue(resultat instanceof String);
    }
}
